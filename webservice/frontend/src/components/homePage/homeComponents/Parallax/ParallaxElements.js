import styled from "styled-components";

export const ParallaxWrapper = styled.section`
  position: relative;
  width: 100%;
  height: 100vh;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;

  &:before {
    content: "";
    position: absolute;
    bottom: 0px;
    width: 100%;
    height: 100px;
    background: linear-gradient(to top, #232627, transparent);
    z-index: 100;
  }

  @media screen and (max-width: 1000px) {
    height: 50vh;
  }
  @media screen and (max-width: 479px) {
    height: 300px;
  }
`;

export const ParallaxBackground = styled.div`
  background: #232627;
  width: 100%;
  height: 100%;
`;

export const ParallaxMovingImg = styled.img`
  position: absolute;
  // starting distance from the top is passed as props
  top: ${(props) => `${props.topDistance}px`};
  // width is specified by props
  width: ${(props) => props.width};
`;

export const ParallaxLogo = styled.img`
  position: absolute;
  top: 110px;
  width: 33vw;

  @media screen and (max-width: 1000px) {
    top: 100px;
    width: 40vw;
  }

  @media screen and (max-width: 479px) {
    top: 80px;
    width: 55vw;
  }
`;

export const ParallaxStaticImg = styled.img`
  position: absolute;
  top: 80px;
  width: 100%;
`;
