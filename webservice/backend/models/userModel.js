const mongoose = require("mongoose");

/**
 * This is schema for user in database
 * @param email
 * @param password is stored as hash
 * @param nickname
 */
const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
  },
  nickname: {
    type: String,
    required: true,
    unique: true,
  },
});

module.exports = User = mongoose.model("user", userSchema);
