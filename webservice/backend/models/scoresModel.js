const mongoose = require("mongoose");

/**
 * Schema for single document in scores array
 * @param player_id is id of player from users collection
 * @param score is score of player on given map
 */
const scoreSchema = new mongoose.Schema({
    player_id: { 
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    score: {
        type: Number,
        required: true
    }
});

/**
 * This is schema for  in database
 * @param map_id is id of map stored in maps collection 
 * @param scores is array of documents, each scores document
 *  contains user_id and his score on this map
 */
const scoresSchema = new mongoose.Schema({
    map_id: { 
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    scores: [scoreSchema]
});

module.exports = Scores = mongoose.model("scores", scoresSchema);