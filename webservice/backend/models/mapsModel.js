const mongoose = require("mongoose");


/**
 * This is schema for maps in database
 * @param name holds name of map
 * @param description holds description of map
 * @param maps holds map file in binary representation
 * @param image holds thumbnail of map in binary representation
 */
const mapsSchema = new mongoose.Schema({
    name: { 
        type: String,
        required: true,
        unique: true 
    },
    description: {
        type: String,
        required: true
    },
    map_hash : {
        type: String,
        required: true
    },
    map: {
        type: String,
        data: Buffer,
        required: true
    },
    image: {
        type: String,
        data: Buffer
    }
});

module.exports = Maps = mongoose.model("maps", mapsSchema);