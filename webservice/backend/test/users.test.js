const { assert } = require("chai");
let chai = require("chai");
let chainHttp = require("chai-http");
let server = require("../index");
const User = require("../models/userModel");

chai.should();

chai.use(chainHttp);

describe("Users", () => {
  describe("POST methods", () => {
    describe("Register method", () => {
      it("New user not all field entered", (done) => {
        const new_user = {
          email: "teddy@email.com",
          password: "qqqqq",
          passwordCheck: "qqqqq",
        };
        chai
          .request(server)
          .post("/users/register")
          .send(new_user)
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have
              .property("msg")
              .eq("Not all fields have been entered");
            done();
          });
      });
      it("New user to short password", (done) => {
        const new_user = {
          email: "teddy@email.com",
          password: "qqqq",
          passwordCheck: "qqqqq",
          nickname: "teddy",
        };
        chai
          .request(server)
          .post("/users/register")
          .send(new_user)
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have
              .property("msg")
              .eq("The password needs to be at least 5 characters long");
            done();
          });
      });
      it("New user password do not match", (done) => {
        const new_user = {
          email: "teddy@email.com",
          password: "qqqqq",
          passwordCheck: "qqqqe",
          nickname: "teddy",
        };
        chai
          .request(server)
          .post("/users/register")
          .send(new_user)
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have
              .property("msg")
              .eq("Enter the same password twice");
            done();
          });
      });
      it("New user register", (done) => {
        const new_user = {
          email: "teddy@email.com",
          password: "qqqqq",
          passwordCheck: "qqqqq",
          nickname: "teddy",
        };
        chai
          .request(server)
          .post("/users/register")
          .send(new_user)
          .end(async (err, res) => {
            res.should.have.status(200);
            res.body.should.have.property("msg").eq("Succes");

            const user = await User.findOne({ email: "teddy@email.com" });
            await User.findByIdAndDelete(user._id);

            done();
          });
      });
    });
    describe("Login method", () => {
      it("Bad email", (done) => {
        const user = {
          email: "benny@email.com",
          password: "qqqqq",
        };
        chai
          .request(server)
          .post("/users/login")
          .send(user)
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have
              .property("msg")
              .eq("Invalid email or password");
            done();
          });
      });
      it("Bad password", (done) => {
        const user = {
          email: "penny@email.com",
          password: "qqqqe",
        };
        chai
          .request(server)
          .post("/users/login")
          .send(user)
          .end(async (err, res) => {
            res.should.have.status(400);
            res.body.should.have
              .property("msg")
              .eq("Invalid email or password");
            done();
          });
      });
      it("Login succes", (done) => {
        const user = {
          email: "penny@email.com",
          password: "qqqqq",
        };
        chai
          .request(server)
          .post("/users/login")
          .send(user)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property("token");
            res.body.should.have.property("msg").eq("Succesfully logged");
            res.body.should.have.property("token");
            done();
          });
      });
    });
    describe("Token authorization", () => {
      //false token
      true_token =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmVjYTM3NWU5OWFhMDZjNDljM2I1YyIsImlhdCI6MTYxMDU1OTAyMn0.yO5QomldbYAOs_rKnVePuRSapIrKPpeAnnrU4ifGxVg";
      false_token =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmVjYTM3NWU5OWFhMDZjNDleM2I1YyIsImlhdCI6MTYxMDU1OTAyMn0.yO5QomldbYAOs_rKnVePuRSapIrKPpeAnnrU4ifGxVg";

      it("/users/tokenIsValid", (done) => {
        chai
          .request(server)
          .post("/users/tokenIsValid")
          .set("x-auth-token", false_token)
          .end(async (err, res) => {
            res.body.should.have.property("error");
            done();
          });
      });
      it("Good token validation", (done) => {
        chai
          .request(server)
          .post("/users/tokenIsValid")
          .set("x-auth-token", true_token)
          .end((err, res) => {
            assert(res.body);
            done();
          });
      });
    });
  });
  describe("Get user data method", () => {
    it("Get user data", (done) => {
      true_token =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmVjYTM3NWU5OWFhMDZjNDljM2I1YyIsImlhdCI6MTYxMDU1OTAyMn0.yO5QomldbYAOs_rKnVePuRSapIrKPpeAnnrU4ifGxVg";
      chai
        .request(server)
        .get("/users/")
        .set("x-auth-token", true_token)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property("nickname");
          res.body.should.have.property("email");
          done();
        });
    });
    it("Get all user data", (done) => {
      true_token =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmVjYTM3NWU5OWFhMDZjNDljM2I1YyIsImlhdCI6MTYxMDU1OTAyMn0.yO5QomldbYAOs_rKnVePuRSapIrKPpeAnnrU4ifGxVg";
      chai
        .request(server)
        .get("/users/allData")
        .set("x-auth-token", true_token)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property("nickname");
          res.body.should.have.property("email");
          res.body.should.have.property("overall_score");
          done();
        });
    });
  });
  describe("PUT methods", () => {
    true_token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmVjYTM3NWU5OWFhMDZjNDljM2I1YyIsImlhdCI6MTYxMDU1OTAyMn0.yO5QomldbYAOs_rKnVePuRSapIrKPpeAnnrU4ifGxVg";
    it("Change name", (done) => {
      const user = { newName: "menny" };
      chai
        .request(server)
        .put("/users/changeName")
        .set("x-auth-token", true_token)
        .send(user)
        .end(async (err, res) => {
          res.should.have.status(200);
          res.body.should.have.property("nickname").eq("menny");
          res.body.should.have.property("msg").eq("Nickname updated");

          await User.findByIdAndUpdate("5ffeca375e99aa06c49c3b5c", {
            nickname: "penny",
          });
          done();
        });
    });
  });
});
