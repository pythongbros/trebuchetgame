const { assert } = require("chai");
let chai = require("chai");
let chainHttp = require("chai-http");
let server = require("../index");
const User = require("../models/userModel");

chai.should();

chai.use(chainHttp);

describe("Scores", () => {
  true_token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmVjYTM3NWU5OWFhMDZjNDljM2I1YyIsImlhdCI6MTYxMDU1OTAyMn0.yO5QomldbYAOs_rKnVePuRSapIrKPpeAnnrU4ifGxVg";
  describe("Update score", () => {
    it("Score updated", (done) => {
      const score_info = {
        map_id: "5ffecabb5e99aa06c49c3b61",
        map_hash:
          "7e2c859310800d2f452515731357b30378bc6c948a5515591219ceb4106e06e0",
        score_value: 45,
      };
      chai
        .request(server)
        .put("/scores/update_score")
        .set("x-auth-token", true_token)
        .send(score_info)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property("msg");
          done();
        });
    });
    it("Bad hash", (done) => {
      const score_info = {
        map_id: "5ffecabb5e99aa06c49c3b61",
        map_hash:
          "7e2c859310800d2f452515731357b30h78bc6c948a5515591219ceb4106e06e0",
        score_value: 45,
      };
      chai
        .request(server)
        .put("/scores/update_score")
        .set("x-auth-token", true_token)
        .send(score_info)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.have
            .property("msg")
            .eq("Hash does not match hash in database");
          done();
        });
    });
    it("Bad map id", (done) => {
      const score_info = {
        map_id: "5ffecabe5e99aa06c49c3b61",
        map_hash:
          "7e2c859310800d2f452515731357b30h78bc6c948a5515591219ceb4106e06e0",
        score_value: 45,
      };
      chai
        .request(server)
        .put("/scores/update_score")
        .set("x-auth-token", true_token)
        .send(score_info)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.have.property("msg").eq("No map with given id");
          done();
        });
    });
    it("User score", (done) => {
      const score_info = {
        map_id: "5ffecabb5e99aa06c49c3b61",
      };
      chai
        .request(server)
        .post("/scores/user_score")
        .set("x-auth-token", true_token)
        .send(score_info)
        .end((err, res) => {
          res.body.should.have.property("msg").eq("Score on map");
          res.body.should.have.property("score");
          done();
        });
    });
    it("Top_n on map", (done) => {
      const score_info = {
        map_id: "5ffecabb5e99aa06c49c3b61",
        top_n: 2,
      };
      chai
        .request(server)
        .post("/scores/map_top")
        .send(score_info)
        .end((err, res) => {
          res.body.should.have.property("msg").eq("Best scores");
          res.body.should.have.property("scores");
          res.body["scores"].should.be.a("array");
          done();
        });
    });
    it("Overall score", (done) => {
      chai
        .request(server)
        .get("/scores/overall_score")
        .set("x-auth-token", true_token)
        .end((err, res) => {
          res.body.should.have.property("msg").eq("Overall score");
          res.body.should.have.property("overall_score");
          done();
        });
    });
  });
});
