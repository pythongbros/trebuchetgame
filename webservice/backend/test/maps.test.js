let chai = require("chai");
let chainHttp = require("chai-http");
let server = require("../index");
const Map = require("../models/mapsModel");

chai.should();

chai.use(chainHttp);

describe("Maps", () => {
  describe("GET methods", () => {
    it("Return all maps", (done) => {
      chai
        .request(server)
        .get("/maps/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body[0].should.have.property("name");
          res.body[0].should.have.property("_id");
          res.body[0].should.have.property("description");
          res.body[0].should.have.property("image");
          res.body.length.should.be.eq(2);
          done();
        });
    });

    it("Return all_maps_with_best_scores", (done) => {
      chai
        .request(server)
        .get("/maps/all_maps_with_best_scores")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body[0].should.have.property("name");
          res.body[0].should.have.property("_id");
          res.body[0].should.have.property("description");
          res.body[0].should.have.property("image");
          res.body[0].should.have.property("best_scores");
          res.body.length.should.be.eq(2);
          done();
        });
    });
    it("Download map", (done) => {
      chai
        .request(server)
        .get("/maps/map_id")
        .set("map_id", "5ffeca935e99aa06c49c3b5f") //id of map warzone
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property("name").eq("Warzone");
          res.body.should.have.property("_id");
          res.body.should.have.property("description");
          res.body.should.have.property("image");
          res.body.should.have.property("map");
          done();
        });
    });

    it("Download map with bad id", (done) => {
      chai
        .request(server)
        .get("/maps/map_id")
        .set("map_id", "5ffeca935e99aa06cb5f") //bad id
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.have.property("msg").eq("No map with given id");
          done();
        });
    });
  });

  describe("POST methods", (done) => {
    it("Add map", (done) => {
      const new_map = {
        name: "New map",
        description: "New map for test",
        map: "a4 c1 45 94 ff 6f 21",
        image: "4a ar b3 4f",
      };
      chai
        .request(server)
        .post("/maps/addmap")
        .send(new_map)
        .end(async (err, res) => {
          await Map.findByIdAndDelete(res.body.map_id);

          res.should.have.status(201);
          res.body.should.have.property("map_id");
          res.body.should.have.property("msg").eq("Map saved");
          done();
        });
    });
    it("Add map with existing name", (done) => {
      const new_map = {
        name: "Warzone",
        description: "New map for test",
        map: "a4 c1 45 94 ff 6f 21",
        image: "4a ar b3 4f",
      };
      chai
        .request(server)
        .post("/maps/addmap")
        .send(new_map)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.have
            .property("msg")
            .eq("Map with given name already exist");
          done();
        });
    });
  });
});
