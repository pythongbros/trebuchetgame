/**
 * Server for application Spachet
 * Server handles requests for both
 * webpage and client game
 *
 * To communicate with clients
 * server uses REST API
 *
 * used libraries
 *
 * express.js
 * mongoose
 * cors
 *
 * used technologies
 * MongoDB
 * expres.js
 */

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();

//setup express
const app = express();
//middleware to interact with any route with express
//json body parser - we can read json objects from
//the requests that we send to express
app.use(express.json());
app.use(cors());

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

//setup mongoose

/*
mongoose.connect(
  process.env.MONGODB_CONNECTION_STRING,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  },
  (err) => {
    if (err) throw err;
    console.log("MongoDB connection established");
  }
);
*/

/*
 * Connection with local database named spachetdb
 */
mongoose.connect(
  "mongodb://localhost/spachetdb",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  },
  (err) => {
    if (err) throw err;
    console.log("Connected to MongoDB");
  }
);
mongoose.set("useFindAndModify", false);

let db = mongoose.connection;

/**
 * setup routes
 * any request with "/x" it will run xRouter middleware
 */
app.use("/users", require("./routes/userRouter"));
app.use("/maps", require("./routes/mapsRouter"));
app.use("/scores", require("./routes/scoresRouter"));
app.use("/download", require("./routes/downloadRouter"));

module.exports = app;
