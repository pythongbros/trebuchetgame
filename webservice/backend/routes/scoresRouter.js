const router = require("express").Router();
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const Scores = require("../models/scoresModel");
const Map = require("../models/mapsModel");
const User = require("../models/userModel");
const crypto = require("crypto");
const mongoose = require("mongoose");

/**
 * PUT method for updating scores on map
 *
 * @param {
 *       map_id: ObjectId,
 *       score_value: Int,
 *       map_hash: String
 *      } req.body
 * @param {
 *          token: String
 *        } req.header
 *
 * @function auth authrises user and return his id read from token
 *
 *
 * Functions retrives data from request body
 *
 * Users recent score is searched in database
 * When no score found, new score is added
 * When old score found, score is updated
 *
 * Status code 200 is returnend
 * And json with message
 *
 * @return {msg: String} res
 * @return {Int} status
 */
router.put("/update_score", auth, async (req, res) => {
  try {
    const map_id = req.body.map_id;
    const user_id = req.user;
    const score_value = req.body.score_value;
    const map_hash = req.body.map_hash;

    const valid_id = mongoose.Types.ObjectId.isValid(map_id);

    if (!valid_id) return res.status(400).json({ msg: "No map with given id" });

    const requestedMap = await Map.findOne({ _id: map_id });

    if (!requestedMap)
      return res.status(400).json({ msg: "No map with given id" });

    if (requestedMap.map_hash != map_hash)
      return res
        .status(400)
        .json({ msg: "Hash does not match hash in database" });

    map_score = await Scores.findOne(
      { map_id: map_id, "scores.player_id": user_id },
      {
        scores: {
          $elemMatch: {
            player_id: user_id,
          },
        },
      }
    );

    if (!map_score) {
      const player_score = {
        player_id: user_id,
        score: score_value,
      };
      existingMap = await Scores.findOne({ map_id: map_id });

      existingMap.scores.push(player_score);
      await existingMap.save();
      return res.status(200).json({ msg: "First score on map added" });
    }

    last_score = map_score.scores[0].score;

    if (last_score >= score_value)
      return res.status(200).json({ msg: "No record, no new score" });

    await Scores.updateOne(
      {
        map_id: map_id,
        "scores.player_id": user_id,
      },
      {
        $set: {
          "scores.$.score": score_value,
        },
      }
    );

    return res.status(200).json({ msg: "Updated score" });
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

/**
 * POST method checking score on map for requested player
 *
 * @param {
 *       map_id: ObjectId,
 *      } req.body
 * @param {
 *          token: String
 *        } req.header
 *
 * @function auth authorises user and return his id read from token
 *
 * Map is searched id database
 * If not exist code 400 and message is returned
 *
 * Then user score on map is checked
 * If not exist code 400 and message is returned
 *
 * If score exist
 * Status code 200 is returnend
 * And json with message and score
 *
 * @return {
 *            score: Int
 *            msg: String
 *         } res
 * @return {Int} status
 */
router.post("/user_score", auth, async (req, res) => {
  try {
    const map_id = req.body.map_id;
    const user_id = req.user;

    const valid_id = mongoose.Types.ObjectId.isValid(map_id);

    if (!valid_id) return res.status(400).json({ msg: "No map with given id" });

    const requestedMap = await Map.findOne({ _id: map_id });
    if (!requestedMap) {
      return res.status(400).json({ msg: "No map with given id" });
    }

    map_score = await Scores.findOne(
      { map_id: map_id, "scores.player_id": user_id },
      {
        scores: {
          $elemMatch: {
            player_id: user_id,
          },
        },
      }
    );

    if (!map_score) {
      return res.status(400).json({ msg: "No score on map" });
    }
    const score = map_score.scores[0].score; //take first element from scores array
    //because map_score.scores is one element array
    return res.status(200).json({ score: score, msg: "Score on map" });
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

/**
 * POST method checking for score of n best players on map
 *
 * @param {
 *       map_id: ObjectId,
 *       top_n:  Int
 *      } req.body
 *
 * Map is searched id database
 * If not exist code 400 and message is returned
 *
 * If map exist
 * N best scores is downoaded from database
 *
 * Then id of player are compared with users names
 * and object of nickname and score are created
 *
 * @return {
 *            scores:
 *            [
 *              player: String,
 *              score: Int
 *            ]
 *            msg: String
 *         } res
 * @return {Int} status
 */
router.post("/map_top", async (req, res) => {
  try {
    const map_id = req.body.map_id;
    const top_n = req.body.top_n;

    const valid_id = mongoose.Types.ObjectId.isValid(map_id);

    if (!valid_id) return res.status(400).json({ msg: "No map with given id" });

    const requestedMap = await Map.findOne({ _id: map_id });
    if (!requestedMap) {
      return res.status(400).json({ msg: "No map with given id" });
    }

    const ObjectId = mongoose.Types.ObjectId;
    map_score = await Scores.aggregate([
      {
        $match: {
          map_id: ObjectId(map_id),
        },
      },
      {
        $unwind: "$scores",
      },
      {
        $sort: {
          "scores.score": -1,
        },
      },
      {
        $group: {
          _id: "$_id",
          scores: {
            $push: "$scores",
          },
        },
      },
    ]);

    if (map_score.length < 1)
      return res.status(200).json({ scores: [], msg: "Best scores" });

    let players_scores = map_score[0]["scores"].slice(0, top_n);
    let players_id = [];
    players_scores.forEach((element) => {
      players_id.push(ObjectId(element["player_id"]));
    });
    users = await User.find({ _id: { $in: players_id } });
    scores = [];
    players_scores.forEach((element) => {
      let player_name = users.find(
        (el) => el["_id"].toString() === element["player_id"].toString()
      );
      let user_record = {
        player: player_name["nickname"],
        score: element["score"],
      };
      if (typeof scores !== "undefined") scores.push(user_record);
      else scores.push(user_record);
    });

    return res.status(200).json({ scores: scores, msg: "Best scores" });
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

/**
 * GET method checking overall score on all maps for given player
 *
 * @param {
 *       x-auth-token: ObjectId,
 *      } req.head
 *
 *  method find scores of player on all maps
 *  query returns array of all maps than user
 *  has score on
 *
 *  then array in iterated,
 *  and value of field scores.score is added
 *  to overall score
 *
 *  If player has no score on any map
 *  then just 0 is retutned
 *
 *  * @return {
 *            overall_score: Int,
 *            msg: String
 *         } res
 * @return {Int} status
 */
router.get("/overall_score", auth, async (req, res) => {
  try {
    const user_id = req.user;

    scores = await Scores.find(
      { "scores.player_id": user_id },
      {
        scores: {
          $elemMatch: {
            player_id: user_id,
          },
        },
      }
    );

    let overall_score = 0;
    scores.forEach((element) => {
      overall_score += element.scores[0].score;
    });

    return res
      .status(200)
      .json({ overall_score: overall_score, msg: "Overall score" });
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

module.exports = router;
