const router = require("express").Router();
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const User = require("../models/userModel");

/**
 * POST method to handle register request
 *
 * @param {
 *          email: String,
 *          password: String,
 *          passwordCheck: String,
 *          nickname: String
 *        } req.body containts data from request
 *
 * Functions handlling register request
 * Gets data from req.body
 * Than validate it
 *
 * Next step is searching for database if
 * given email already exist
 * If no error, user is registered and stored in database
 *
 * @return {
 *          msg: String
 *         } res params from respone to cilient
 */
router.post("/register", async (req, res) => {
  try {
    let { email, password, passwordCheck, nickname } = req.body;
    //validate
    //400 status - bad request
    if (!email || !password || !passwordCheck || !nickname)
      return res.status(400).json({ msg: "Not all fields have been entered" });
    if (password.length < 5)
      return res
        .status(400)
        .json({ msg: "The password needs to be at least 5 characters long" });
    if (password !== passwordCheck)
      return res.status(400).json({ msg: "Enter the same password twice" });

    let existingUser = await User.findOne({ email: email });

    if (existingUser) {
      return res
        .status(400)
        .json({ msg: "An account with this email already exists" });
    }

    existingUser = await User.findOne({ nickname: nickname });

    if (existingUser) {
      return res.status(400).json({ msg: "This name is already taken" });
    }

    const newUser = new User({
      email,
      password,
      nickname,
    });

    const savedUser = await newUser.save();
    return res.status(200).json({ msg: "Succes" });
  } catch (err) {
    //500 means internal error
    res.status(500).json({ error: err.message });
  }
});

/**
 * POST method to handling login mechanism
 *
 * @param {
 *          email: String,
 *          password: String
 *        } req.body req containts data from request
 *
 * Method validates if all required field are entered
 * Then user is searched in database
 * When email and password agree
 * Token and nickname is sent as response
 *
 * @returns {
 *            token: String,
 *            nickname: String,
 *            msg: String
 *          } res
 */
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    //validate
    if (!email || !password)
      return res.status(400).json({ msg: "Not all fields have been entered" });

    const user = await User.findOne({ email: email });
    if (!user)
      return res.status(400).json({ msg: "Invalid email or password" });

    if (password != user.password)
      return res.status(400).json({ msg: "Invalid email or password" });

    const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET);

    res.status(200).json({
      token,
      nickname: user.nickname,
      msg: "Succesfully logged",
    });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * DELETE method to handling login mechanism
 *
 * @param {
 *          x-auth-token: String
 *        } req.header req containts data from request
 *
 * Method validates usin auth middleware if token is valid
 * Then user is searched in database and deleted
 * When deleted res is returned
 *
 * @returns {
 *            nickname: String,
 *            msg: String
 *          } res
 */
router.delete("/delete", auth, async (req, res) => {
  //we want this callback to be run only when we
  //know that the user who wants to perfom the deletion
  //is logged in (so basically when we validated the json web token)
  //we gonna do that by using some middleware - auth function
  try {
    //mongoose returns the item which has been deleted
    const deletedUser = await User.findByIdAndDelete(req.user);
    //send deleted user to the front
    //to say goodbye "user123" or whatever
    return res
      .status(200)
      .json({ nickname: deletedUser.nickname, msg: "User deleted" });
  } catch (err) {
    //500 - internal server error
    res.status(500).json({ error: err.message });
  }
});

/**
 * PUT method for changing nickname
 *
 * @param {
 *          newName: String
 *        } req.body
 * @param {
 *          x-auth-token: String,
 *        } req.header
 *
 * @function auth authorises user and return his id read from token
 *
 * Method validates usin auth middleware if token is valid
 * Then function check if there exist user with new Name,
 * If not, name of requested user is changed
 *
 * @returns {
 *            nickname: String,
 *            msg: String
 *          } res
 */
router.put("/changeName", auth, async (req, res) => {
  //this callback will be executed only when user is authorized
  //(same as in the delete route)
  try {
    const newName = req.body.newName;

    if (!newName) return res.status(400).json({ msg: "No new name provided" });

    //check if newName is already taken
    const userWithNewName = await User.findOne({ nickname: newName });

    //if some user already has this name
    if (userWithNewName) {
      //406 - Not acceptable
      return res.status(406).json({ msg: "This name is already taken" });
    }

    //if not taken then update the user name
    await User.findByIdAndUpdate(req.user, {
      nickname: newName,
    });

    //return updated user to the frontend
    const user = await User.findById(req.user);
    return res.status(200).json({
      nickname: user.nickname,
      msg: "Nickname updated",
    });
  } catch (err) {
    //500 - internal server error
    res.status(500).json({ error: err.msg });
  }
});

/**
 * PUT method for changing email
 *
 * @param {
 *          newEmail: String
 *        } req.body
 * @param {
 *          x-auth-token: String,
 *        } req.header
 *
 * @function auth authorises user and return his id read from token
 *
 * Method validates usin auth middleware if token is valid
 * Then function check if there exist user with new email,
 * If not, email of requested user is changed
 *
 * @returns {
 *            email: String,
 *            msg: String
 *          } res
 */
router.put("/changeEmail", auth, async (req, res) => {
  try {
    const newEmail = req.body.newEmail;

    if (!newEmail)
      return res.status(400).json({ msg: "No new email provided" });
    const userWithNewEmail = await User.findOne({ email: newEmail });

    if (userWithNewEmail) {
      return res.status(406).json({ msg: "This email is already taken" });
    }

    await User.findByIdAndUpdate(req.user, {
      email: newEmail,
    });

    const user = await User.findById(req.user);
    return res.status(200).json({
      email: user.email,
      msg: "Email updated",
    });
  } catch (err) {
    res.status(500).json({ error: err.msg });
  }
});

/**
 * GET methods that returns user data
 *
 * @param {} req.body containts data from request
 * @param {
 *          x-auth-token: String
 *        } req.header
 *
 * @function auth authorises user and return his id read from token
 *
 * @returns {
 *            nickname: String
 *          } res
 */

router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findById(req.user);
    res.status(200).json({
      nickname: user.nickname,
      email: user.email,
    });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * POST check if recived token is valid
 * It is used when user opens website
 * And token from local storage is verivied
 *
 * @param {
 *          x-auth-token: String
 *        } req.header
 *
 * @function auth authorises user and return his id read from token
 * @returns {boolean} res
 */
router.post("/tokenIsValid", async (req, res) => {
  try {
    //token from frontend if user logged in
    const token = req.header("x-auth-token");
    //if token not provided - token invalid
    if (!token) return res.json(false);

    //jwt.verify returns decoded token if token correct
    //if not verified it will return null
    const verified = jwt.verify(token, process.env.JWT_SECRET);

    if (!verified) {
      return res.json(false);
    }

    //find user with id coded in the token
    const user = await User.findById(verified.id);

    //if token was verified but there is no user with such id
    //token is still invalid
    if (!user) return res.json(false);
  } catch (err) {
    //500 - internal server error
    res.status(500).json({ error: err.message });
  }

  //if none of the above happend then token is verified
  return res.json(true);
});

/**
 * PUT method for changingPassword
 *
 * @param {
 *          newPassword: String
 *        } req.body
 * @param {
 *          x-auth-token: String,
 *        } req.header
 *
 * @function auth authorises user and return his id read from token
 *
 * Method validates using auth middleware if token is valid
 * Then function validates password
 * When passwords passes validation
 * Password is changed in database
 *
 *
 * @returns {
 *            msg: String
 *          } res
 */
router.put("/changePassword", auth, async (req, res) => {
  try {
    const newPassword = req.body.newPassword;
    console.log(newPassword);
    console.log(req.user);
    if (!newPassword)
      return res.status(400).json({ msg: "No new password provided" });
    if (newPassword.length < 5)
      return res
        .status(400)
        .json({ msg: "The password needs to be at least 5 characters long" });
    await User.findByIdAndUpdate(req.user, {
      password: newPassword,
    });

    return res.status(200).json({
      msg: "Password updated",
    });
  } catch (err) {
    res.status(500).json({ error: err.msg });
  }
});

/**
 * GET method check overall info about user
 *
 * @param {
 *       x-auth-token: ObjectId,
 *      } req.head
 *
 *  method find scores of player on all maps
 *  query returns array of all maps than user
 *  has score on
 *
 *  then array in iterated,
 *  and value of field scores.score is added
 *  to overall score
 *
 *  If player has no score on any map
 *  then just 0 is retutned
 *
 *  Together with score,
 *  nickname of user and email is returned
 *
 *  * @return {
 *            overall_score: Int,
 *            nickname: String,
 *            email: String,
 *            msg: String
 *         } res
 * @return {Int} status
 */
router.get("/allData", auth, async (req, res) => {
  try {
    const user_id = req.user;

    scores = await Scores.find(
      { "scores.player_id": user_id },
      {
        scores: {
          $elemMatch: {
            player_id: user_id,
          },
        },
      }
    );

    let overall_score = 0;
    scores.forEach((element) => {
      overall_score += element.scores[0].score;
    });

    const user = await User.findById(req.user);

    return res.status(200).json({
      overall_score: overall_score,
      nickname: user.nickname,
      email: user.email,
      msg: "User data",
    });
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

module.exports = router;
