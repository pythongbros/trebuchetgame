const router = require("express").Router();

/** GET route
 *  Enables to download game from server
 *
 *  @return {file} res.dowload
 */
router.get("/", async (req, res) => {
  try {
    const file = "./files/trebuchetgame.zip";
    return res.download(file);

  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

module.exports = router;
