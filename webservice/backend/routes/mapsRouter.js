const router = require("express").Router();
const Map = require("../models/mapsModel");
const Score = require("../models/scoresModel");
const User = require("../models/userModel");
const crypto = require("crypto");
const mongoose = require("mongoose");

/**
 * POST method for adding maps to database
 *
 * @param {
 *          name: String,
 *          descritpion: String,
 *          map: Buffer String(BLOB),
 *          image: Buffer String(BLOB)
 *        } req.body
 *
 *
 * Functions retrives data from request body
 * Check if all requred field provided and
 * if given name for map is free.
 *
 * When error code 400 returned
 *
 * If no error
 *
 * Document is saved in maps collection
 * And map id is saved in scores colletion
 * Then returns code 200
 *
 * @return {
 *          msg: String
 *         } res
 * @return {Int} status
 */
router.post("/addmap", async (req, res) => {
  try {
    let { name, description, map, image } = req.body;

    if (!name || !description)
      return res.status(400).json({ msg: "No map name or description" });
    if (!map) return res.status(400).json({ msg: "No map file provided" });

    map_hash = crypto.createHash("sha256").update(map).digest("hex");

    const existingMap = await Map.findOne({ name: name });

    if (existingMap)
      return res.status(400).json({ msg: "Map with given name already exist" });

    const newMap = new Map({
      name,
      description,
      map_hash,
      map,
      image,
    });

    dbmap = await newMap.save();

    const map_id = dbmap._id;

    const newScoresDocument = new Score({
      map_id: map_id,
    });

    await newScoresDocument.save();

    return res.status(201).json({
      map_id: map_id,
      msg: "Map saved",
    });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * GET method for available maps in database
 *
 * @param {} req.body
 *
 * Function return informations about
 * all maps stored in database
 * Code 200 is returned
 *
 * Might return null if no map in database
 *
 * @return {
 *          [
 *           _id: ObjectId,
 *           name: String,
 *           description: String,
 *           image: Buffer String(BLOB)
 *       ]
 *         } res
 * @return {Int} status
 */
router.get("/", async (req, res) => {
  try {
    const existingMaps = await Map.find({}, "name description image");

    return res.status(200).json(existingMaps);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * GET method for available maps in database with best scores of players
 *
 * @param {} req.body
 *
 * Function return informations about
 * all maps stored in database
 * Code 200 is returned
 *
 * Might return null if no map in database
 *
 * @return {
 *          [
 *           _id: ObjectId,
 *           name: String,
 *           description: String,
 *           image: Buffer String(BLOB),
 *           best_scores: [
 *                          {
 *                            player: String,
 *                            score: Int
 *                           }
 *                        ]
 *       ]
 *         } res
 * @return {Int} status
 */

router.get("/all_maps_with_best_scores", async (req, res) => {
  try {
    const existingMaps = await Map.find({}, "name description image");
    top_n = 5;
    best_maps = [];
    if (existingMaps.length < 1) {
      return res.status(200).json([]);
    }
    async function processArray(array) {
      for (let element of array) {
        const ObjectId = mongoose.Types.ObjectId;
        map_id = element._id;
        map_score = await Scores.aggregate([
          {
            $match: {
              map_id: ObjectId(map_id),
            },
          },
          {
            $unwind: "$scores",
          },
          {
            $sort: {
              "scores.score": -1,
            },
          },
          {
            $group: {
              _id: "$_id",
              scores: {
                $push: "$scores",
              },
            },
          },
        ]);
        if (map_score.length < 1) {
          let tmp_element = element.toObject();
          tmp_element.best_scores = [];
          best_maps.push(tmp_element);
        } else {
          let players_scores = map_score[0]["scores"].slice(0, top_n);
          let players_id = [];
          players_scores.forEach((element) => {
            players_id.push(ObjectId(element["player_id"]));
          });
          users = await User.find({ _id: { $in: players_id } });

          scores = [];
          players_scores.forEach((element) => {
            let player_name = users.find(
              (el) => el["_id"].toString() === element["player_id"].toString()
            );
            let user_record = {
              player: player_name["nickname"],
              score: element["score"],
            };

            scores.push(user_record);
          });
          let tmp_element = element.toObject();
          tmp_element.best_scores = scores;
          best_maps.push(tmp_element);
        }
      }
      return res.status(200).json(best_maps);
    }

    processArray(existingMaps);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * GET method download map from database
 *
 * @param {
 *          map_id: ObjectId
 *        } res.header
 *
 * Function return map with given id if map in database
 * When map not found, return code 400
 *
 * When map exist return map and code 200
 *
 * @return {
 *          [
 *           _id: ObjectId,
 *           name: String,
 *           description: String,
 *           map_hash: String,
 *           map: Buffer String(BLOB),
 *           image: Buffer String(BLOB),
 *           _v: Int
 *       ]
 *         } res
 * @return {Int} status
 */
router.get("/map_id", async (req, res) => {
  try {
    let map_id = req.header("map_id");

    const valid_id = mongoose.Types.ObjectId.isValid(map_id);

    if (!valid_id) return res.status(400).json({ msg: "No map with given id" });
    const existingMap = await Map.findOne({ _id: map_id });

    if (!existingMap)
      return res.status(400).json({ msg: "No map with given id" });

    return res.status(200).json(existingMap);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

module.exports = router;
