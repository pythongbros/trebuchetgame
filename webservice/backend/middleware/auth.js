//we need json web token to verify the token
const jwt = require("jsonwebtoken");

/**
 * Middleware to verify iv token is valid
 * @param {
 *          x-auth-token: String
 *        } req.header
 *
 * Functions retrieves token
 * And checks if validation function can verifi token
 * If token is no verified code 4xx is returned and message
 * If token is verified, id of user read from token
 * is passed to requested route
 *
 * @param {
 *          msg: String
 *        } res
 */
const auth = (req, res, next) => {
  try {
    //we will get the token from frontend if the user is logged in (in the header)
    const token = req.header("x-auth-token");

    if (!token)
      //401 - unauthorized if no token send
      return res
        .status(401)
        .json({ msg: "No authentication token, authorization denied" });

    //jwt.verify will return decoded token like so:
    // { id: '5f7defe901f85258d43e493a', iat: 1602334876 }
    const verified = jwt.verify(token, process.env.JWT_SECRET);

    //if someone tried to fake the token it will not be verified
    if (!verified) {
      //401 - unauthorized if token cant be verified with our password aka JWT_SECRET
      return res
        .status(401)
        .json({ msg: "Token verification failed, authorization denied" });
    }

    //if authentication is ok then
    //in the request object add field "user" with the id
    //of the user who sent this req
    req.user = verified.id;
    //next() will let the callback form delete route to be executed
    next();
  } catch (err) {
    //500 - internal server error
    res.status(500).json({ error: err.message });
  }
};

module.exports = auth;
