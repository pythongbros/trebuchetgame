"""
.. module:: main
   :platform: Windows
   :synopsis: Main module to run a game

.. moduleauthor:: PythongBrox
"""
import pyglet

pyglet.options["debug_gl"] = False

from .client_window import ClientWindow


def start_app():
    """Method starting an app"""

    pyglet.font.add_directory("game/resources/fonts")

    window = ClientWindow(1920, 1080, "Spachet", resizable=True)
    window.set_minimum_size(320, 200)

    pyglet.clock.schedule_interval(window.update, 1 / 60.0)
    pyglet.app.run()


if __name__ == "__main__":
    start_app()
