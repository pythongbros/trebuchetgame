from .window import Window
from game.scenes import EditorScene


class EditorWindow(Window):
    """Window for editor app"""

    def __init__(self, width, height, name, **kwargs):
        super().__init__(EditorScene, width, height, name, **kwargs)

    def on_draw(self):
        super().on_draw()

    def update(self, dt):
        super().update(dt)
