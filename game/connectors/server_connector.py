import requests
from requests.exceptions import RequestException
import hashlib
import base64
import json
import os
import io

import pyglet
from pyglet.image import ImageData

from game.maps import Map


class ServerConnector:

    url = "http://localhost:5000"
    token = None
    nickname = None
    email = None

    @staticmethod
    def login(email: str, password: str):
        login_url = ServerConnector.url + "/users/login"
        password_hash = hashlib.sha256(bytearray(password, "utf-8")).hexdigest()

        try:
            r = requests.post(
                login_url, json={"email": email, "password": password_hash}
            )
        except RequestException:
            return 505, "Cannot connect to server"

        req_json = r.json()

        if r.status_code == 200:
            ServerConnector.email = email
            ServerConnector.token = req_json["token"]
            ServerConnector.nickname = req_json["nickname"]

        return r.status_code, req_json["msg"]

    @staticmethod
    def get_maps_data():
        maps_url = ServerConnector.url + "/maps"

        try:
            r = requests.get(maps_url)
        except RequestException:
            return 505, "Cannot connect to server"

        return r.status_code, r.json()

    @staticmethod
    def send_score(map_: Map, score: int):
        score_url = ServerConnector.url + "/scores/update_score"
        map_hash = map_.hash()

        try:
            r = requests.put(
                score_url,
                headers={"x-auth-token": ServerConnector.token},
                json={"map_id": map_._id, "score_value": score, "map_hash": map_hash},
            )

        except RequestException:
            return 505, "Cannot connect to server"

        return r.status_code, r.json()["msg"]

    @staticmethod
    def send_map(_map: Map):
        send_url = ServerConnector.url + "/maps/addmap"

        with open(_map.directory + "/object.json", "rb") as f:
            encoded_map = base64.b64encode(f.read()).decode("utf-8")

        if os.path.exists(image_path := _map.directory + "/image.png"):

            with open(image_path, "rb") as f:
                encoded_image = base64.b64encode(f.read()).decode("utf-8")
        else:
            with open("game/resources/gui/editor/img_planet_default.png", "rb") as f:
                encoded_image = base64.b64encode(f.read()).decode("utf-8")

        try:
            r = requests.post(
                send_url,
                json={
                    "name": _map.name,
                    "description": _map.description,
                    "map": encoded_map,
                    "image": encoded_image,
                },
            )

            with open(_map.directory + "/info.json", "r") as f:
                data = json.load(f)
                data["_id"] = r.json()["map_id"]

            with open(_map.directory + "/info.json", "w") as f:
                json.dump(data, f)

        except RequestException:
            return 505, "Cannot connect to server"

        return r.status_code, r.json()["msg"]

    @staticmethod
    def download_map(_map: Map):
        download_url = ServerConnector.url + "/maps/map_id"

        try:
            r = requests.get(download_url, headers={"map_id": _map._id})

        except RequestException:
            return 505, "Cannot connect to server"

        if r.status_code == 200:
            data = r.json()

            root = f"game/maps/map_files/map_{data['name']}"

            # Creatinh map dir
            while os.path.exists(root):
                root += "1"

            os.mkdir(root)

            # Saving info
            with open(root + "/info.json", "w") as f:
                info = {
                    "_id": data["_id"],
                    "name": data["name"],
                    "description": data["description"],
                }
                json.dump(info, f)

            # Saving map object
            with open(root + "/object.json", "w") as f:
                obj = base64.b64decode(data["map"]).decode("utf-8")
                obj = obj.replace("\r", "")
                f.write(obj)

            # Saving image
            if data["image"] is not None:

                fileobj = base64.b64decode(data["image"])
                image = pyglet.image.load("sample.png", file=io.BytesIO(fileobj))
                image.save(root + "/image.png")

            msg = "Map downloaded"

        else:
            msg = r.json()["msg"]

        return r.status_code, msg
