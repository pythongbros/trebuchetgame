import pyglet
from pyglet.window import FPSDisplay

from .scene import Scene
from .utils import Scroller
from game.system import ObjectManager
from game.window import Window
from game.objects import SceneObject
from game.gui import Counter, Timer, Icon, ProgressBar, ProjectileTray
from game.core import Rectangle
from game.maps import Map


class GameScene(Scene):
    """Main game window with attached pymunk space for physics simulation"""

    def __init__(self, window: Window, map_: Map, obj_manager: ObjectManager, **kwargs):
        super().__init__(window)

        self.window.fps = FPSDisplay(self.window)
        self.scroller = Scroller(self.window.original_dim, 5)

        self.map = map_
        self.obj_manager = obj_manager
        self.obj_manager.window = self.window

        self.timer = Timer(Rectangle(1675, 1025, 1875, 1055), minutes=0)

        self.end_counter = None
        self.all_mobs = self.obj_manager.get_mobs_num()
        self.obj_manager.playing = True
        self.mobs_killed = 0

    def setup(self):
        super().setup()
        self.window.push_handlers(self.scroller.on_key_press)
        self.window.push_handlers(self.scroller.on_key_release)

        self.gui_manager.register(self.timer, "foreground")
        self.timer.start()

        self.obj_manager.next_projectile()

        for projectile_template in self.map.projectile_tray:
            self.projectile_tray.push_projectile(projectile_template)

    def _build_gui(self):
        super()._build_gui()

        self.background.set_image(pyglet.image.load(self.map.background_image_path))

        self.tray_lower = Icon(
            Rectangle(0, 0, 500, 100),
            pyglet.image.load("game/resources/gui/editor/tray_lower.png"),
        )
        self.gui_manager.register(self.tray_lower, "foreground")

        self.milk_bar = ProgressBar(
            Rectangle(50, 65, 360, 80),
            pyglet.image.load("game/resources/gui/editor/bar_milk_frame.png"),
            pyglet.image.load("game/resources/gui/editor/bar_milk_fill.png"),
            100,  # HARDCODED MILK LEVEL!!!
            100,  # HARDCODED MILK LEVEL!!!
            50,
            0,
        )
        self.tray_lower.add_child(self.milk_bar)

        self.projectile_tray = ProjectileTray(Rectangle(10, 10, 460, 50), 8, False)
        self.gui_manager.register(self.projectile_tray.context_menu, "foreground")

        self.tray_lower.add_child(self.projectile_tray)

    def _build_scene(self):
        super()._build_scene()

        img = pyglet.image.load(self.map.planet_image_path)
        planet_offset = -250

        self.planet = SceneObject(img, (0, 0))
        self.planet.permament = True
        self.planet.set_position(
            self.window.original_dim[0] + planet_offset,
            self.planet.image.height / 2,
        )

        self.obj_manager.register(self.planet)
        self.obj_manager.move_to_back(self.planet)

    def start_countdown(self):
        """When there are no more projectiles start countdown"""

        if self.end_counter is None:
            self.end_counter = Counter(Rectangle(910, 460, 1010, 560), init_value=5)
            self.end_counter.set_font_size(35)
            self.gui_manager.register(self.end_counter, "foreground")
            self.gui_manager.notify_resize(self.window.width, self.window.height)

    def end_map(self):
        """When countdown ends navigate to summary scene"""
        from .summary_scene import SummaryScene

        projectiles_left = len(self.obj_manager.projectiles)

        if self.obj_manager.current_projectile is not None:
            projectiles_left += 1

        self.change_scene(
            SummaryScene,
            map_=self.map,
            all_mobs=self.all_mobs,
            mobs_killed=self.mobs_killed,
            time=self.timer.get_secs(),
            projectiles_left=projectiles_left,
        )

    def on_draw(self):
        self.window.clear()
        translation = self.scroller.get_translation()
        self.gui_manager.draw_group("background")
        self.obj_manager.draw_all(translation)
        self.gui_manager.draw_group("foreground")

        self.window.fps.draw()

    def destructor(self):
        super().destructor()
        self.window.remove_handlers(self.scroller.on_key_press)
        self.window.remove_handlers(self.scroller.on_key_release)

    def update(self, dt: float):
        super().update(dt)
        self.scroller.update(self.window.width, self.window.height)

        self.obj_manager.space.step(1 / 60.0)
        self.obj_manager.update_all()

        # Projectile removal
        if len(self.projectile_tray.queue) > len(self.obj_manager.projectiles):
            self.projectile_tray.pop_projectile()

        # Game end counter
        if self.end_counter is not None:
            self.end_counter.add(-dt)

            if self.end_counter.value <= 0:
                self.end_map()
