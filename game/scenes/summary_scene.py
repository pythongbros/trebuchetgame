import logging

import pyglet
from game.core.geometry import Rectangle
from game.gui import Button, TextLabel, Icon

from game.connectors import ServerConnector
from .scene import Scene
from game.window import Window
from game.maps import Map


class SummaryScene(Scene):
    """This is a window representing the game summary scene

    Attributes
    ----------

    """

    def __init__(
        self,
        window: Window,
        map_: Map,
        all_mobs: int,
        mobs_killed: int,
        time: int,
        projectiles_left: int,
        **kwargs,
    ):
        super().__init__(window)
        self.won = mobs_killed == all_mobs

        self.icon = Icon(
            Rectangle(791, 800, 1129, 977),
            img=pyglet.image.load("game/resources/gui/logotitle.png"),
        )
        self.result_label = TextLabel(
            Rectangle(710, 700, 1210, 800),
            "You won" if self.won else "You lost",
            font_size=30,
            halign="center",
            valign="center",
        )

        self.mobs_label = TextLabel(
            Rectangle(710, 560, 1210, 630),
            f"Mobs killed: {mobs_killed}/{all_mobs}",
            font_size=20,
            halign="center",
            valign="center",
        )

        self.time_label = TextLabel(
            Rectangle(710, 490, 1210, 560),
            f"Time passed [s]: {time}",
            font_size=20,
            halign="center",
            valign="center",
        )

        self.projectiles_label = TextLabel(
            Rectangle(710, 420, 1210, 490),
            f"Projectiles left: {projectiles_left}",
            font_size=20,
            halign="center",
            valign="center",
        )

        score = projectiles_left * 200 + mobs_killed * 100 - time // 5

        self.score_label = TextLabel(
            Rectangle(710, 350, 1210, 420),
            f"Score: {score}",
            font_size=20,
            halign="center",
            valign="center",
        )

        self.menu_button = Button(
            Rectangle(860, 220, 1060, 270),
            pyglet.image.load("game/resources/gui/button_violet_def.png"),
            pyglet.image.load("game/resources/gui/button_violet_hov.png"),
            pyglet.image.load("game/resources/gui/button_violet_pre.png"),
            "Main menu",
        )

        self.send_score_button = Button(
            Rectangle(860, 140, 1060, 190),
            pyglet.image.load("game/resources/gui/button_violet_def.png"),
            pyglet.image.load("game/resources/gui/button_violet_hov.png"),
            pyglet.image.load("game/resources/gui/button_violet_pre.png"),
            "Send score",
        )

        self.menu_button.on_lclick_pressed_clbck = self.to_main_menu

        # Function sending score to server
        def send_score(x, y, modifiers, obj):
            code, msg = ServerConnector.send_score(map_, score)
            self.message_label.set_text(msg)

            if code == 200:
                self.message_label.set_color((50, 205, 50, 255))
            else:
                self.message_label.set_color((169, 2, 48, 255))

        self.send_score_button.on_lclick_pressed_clbck = send_score

    def setup(self):
        super().setup()
        self.gui_manager.register(self.icon, "foreground")
        self.gui_manager.register(self.result_label, "foreground")
        self.gui_manager.register(self.mobs_label, "foreground")
        self.gui_manager.register(self.projectiles_label, "foreground")
        self.gui_manager.register(self.time_label, "foreground")
        self.gui_manager.register(self.score_label, "foreground")
        self.gui_manager.register(self.menu_button, "foreground")

        if self.won:
            self.gui_manager.register(self.send_score_button, "foreground")

    def to_main_menu(self, x, y, modifiers, obj):
        from .menu_scene import MenuScene

        self.change_scene(MenuScene)

    def update(self, dt: float):
        super().update(dt)
