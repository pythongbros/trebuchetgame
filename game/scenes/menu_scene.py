import pyglet

from game.connectors import ServerConnector
from .utils import Pager
from .scene import Scene
from game.maps import MapLoader
from game.core.geometry import Rectangle
from game.gui import Button, TextLabel
from game.window import Window
from game.maps import Map, MapState


class MenuScene(Scene):
    """This is a window representing the map choice screen"""

    def __init__(self, window: Window, refresh: bool = False, **kwargs):
        super().__init__(window)
        maps = MapLoader.load_maps_data(refresh)

        self.pager = Pager(
            self, self.gui_manager, maps, (100, 900), (7, 3), (200, 200), (50, 50)
        )

        self.editor_button = Button(
            Rectangle(1670, 1005, 1800, 1055),
            pyglet.image.load("game/resources/gui/button_violet_def.png"),
            pyglet.image.load("game/resources/gui/button_violet_hov.png"),
            pyglet.image.load("game/resources/gui/button_violet_pre.png"),
            "Editor",
        )

        self.profile_label = TextLabel(
            Rectangle(1515, 1005, 1645, 1055),
            ServerConnector.nickname,
            font_size=16,
            halign="center",
            valign="center",
            anchor_x="left",
            anchor_y="bottom",
        )

    def setup(self):
        super().setup()
        self.pager.setup()
        self.editor_button.on_lclick_pressed_clbck = self.editor
        self.gui_manager.register(self.editor_button, "foreground")
        self.gui_manager.register(self.profile_label, "foreground")

    def editor(self, x, y, modifiers, obj):
        from game.editor_window import EditorWindow

        window = EditorWindow(1920, 1080, "Spachet Map Editor", resizable=True)
        window.set_minimum_size(320, 200)
        pyglet.clock.schedule_interval(window.update, 1 / 60.0)
        pyglet.app.run()

    def play(self, map_: Map, state_button: Button):
        from .game_scene import GameScene

        def func(x, y, modifiers, obj):

            state_p1 = state_button.rectangle.lower_left

            if x < state_p1.x and y < state_p1.y:
                obj_manager, message = MapLoader().load_map(
                    self.window.width, self.window.height, map_
                )

                if obj_manager is not None:
                    self.change_scene(
                        GameScene,
                        map_=map_,
                        obj_manager=obj_manager,
                    )
                else:
                    self.message_label.set_text(message)

        return func

    def state_action(self, map_: Map):
        def func(x, y, modifiers, obj):

            if map_.state == MapState.LOCAL:
                code, msg = ServerConnector.send_map(map_)

                if code == 201:
                    self.change_scene(MenuScene, refresh=True)
                else:
                    self.message_label.set_text(msg)

            elif map_.state == MapState.SERVER:
                code, msg = ServerConnector.download_map(map_)

                if code == 200:
                    self.change_scene(MenuScene, refresh=True)
                else:
                    self.message_label.set_text(msg)

        return func

    def update(self, dt: float):
        super().update(dt)
