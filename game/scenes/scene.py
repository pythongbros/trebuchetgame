import pyglet

from game.system import GUIManager, ObjectManager
from game.utils.graphics import scale_image
from game.core.geometry import Rectangle
from game.gui import Button, TextLabel, Icon


class Scene:
    """This is an interface provided to create new Windows (which we will call Scenes)

    Attributes
    ----------
    window: pyglet.Window
        Reference to window where scene is being displayed.

    """

    def __init__(self, window: pyglet.window.Window):
        """Creates new scene"""
        self.window = window
        self.gui_manager = GUIManager(
            self.window.original_dim[0], self.window.original_dim[1]
        )

        self.gui_manager.create_group("background")
        self.gui_manager.create_group("foreground")

    def setup(self):
        """Register all components after change scene called"""
        self.window.push_handlers(self.gui_manager)
        self._build_gui()
        self._build_scene()

    def _build_gui(self):
        """Constructs GUI of this scene"""
        self.background = Icon(
            Rectangle(0, 0, *self.window.original_dim),
            pyglet.image.load("game/resources/gui/background.png"),
        )

        self.exit_button = Button(
            Rectangle(1825, 1005, 1895, 1055),
            pyglet.image.load("game/resources/gui/button_violet_def.png"),
            pyglet.image.load("game/resources/gui/button_violet_hov.png"),
            pyglet.image.load("game/resources/gui/button_violet_pre.png"),
            "Exit",
        )

        self.message_label = TextLabel(
            Rectangle(560, 100, 1360, 200),
            text="",
            font_size=20,
            halign="center",
            valign="center",
            color=(169, 2, 48, 255),
        )

        self.exit_button.on_lclick_pressed_clbck = lambda *_: self.window.close()

        self.gui_manager.register(self.background, "background")
        self.gui_manager.register(self.exit_button, "foreground")
        self.gui_manager.register(self.message_label, "foreground")

    def _build_scene(self):
        """Constructs insides of this scene"""
        pass

    def destructor(self):
        """Destructs the Scene Object"""
        self.gui_manager.destruct()
        self.window.remove_handlers(self.gui_manager)

    def on_draw(self):
        """Method that draws the scene"""
        self.window.clear()
        self.gui_manager.draw_all()

    def update(self, dt: float):
        """This method is called every frame to update the scene"""
        self.gui_manager.update_all()

    def change_scene(self, new_scene, **kwargs):
        self.destructor()
        scene = new_scene(self.window, **kwargs)
        self.window.change_scene(scene)
