from pymunk.vec2d import Vec2d
from pyglet.window import key


class Scroller:
    """Scroller for pyglet window.

    You can use scroller via arrow keys. This can be done
    by simply pressing or holding. Max scroll is equal to
    default dimension of window.

    Attributes
    ----------

    original_dim : tuple
        Original dimension of window.

    speed : int
        Number of pixels to translate per press.
    """

    def __init__(self, original_dim: tuple, speed: int):
        self.original_dim = original_dim
        self.speed = speed

        # Difference between original dim and current dim
        self.clipped_dim = Vec2d(0, 0)

        # Translation value
        self.scroll = Vec2d(0, 0)

        self.key_pressed = None

    def on_key_press(self, symbol, modifiers):
        self.key_pressed = symbol

        # Check either movement in given direction can be done
        # If so do it with a given speed if normalize to bound if necessary
        if symbol == key.RIGHT and self.scroll.x < self.clipped_dim.x:
            self.scroll.x += self.speed
            self.scroll.x = min(self.scroll.x, self.clipped_dim.x)

        elif symbol == key.LEFT and self.scroll.x > 0:
            self.scroll.x -= self.speed
            self.scroll.x = max(self.scroll.x, 0)

        elif symbol == key.UP and self.scroll.y < self.clipped_dim.y:
            self.scroll.y += self.speed
            self.scroll.y = min(self.scroll.y, self.clipped_dim.y)

        elif symbol == key.DOWN and self.scroll.y > 0:
            self.scroll.y -= self.speed
            self.scroll.y = max(self.scroll.y, 0)

    def on_key_hold(self):

        # Simulate holding key in pyglet
        if self.key_pressed is not None:
            self.on_key_press(self.key_pressed, None)

    def on_key_release(self, symbol, modifiers):
        self.key_pressed = None

    def get_translation(self):
        """Returns opengl translation tuple"""
        return (*(-self.scroll), 0)

    def update(self, width: int, height: int):

        # Calculate new clipped window
        self.clipped_dim = Vec2d(
            self.original_dim[0] - width, self.original_dim[1] - height
        )

        # Simulate on_key_hold by calling it with each update
        self.on_key_hold()
