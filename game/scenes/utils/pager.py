import pyglet

from game.system import GUIManager
from game.core.geometry import Rectangle
from game.gui import Button, TextLabel


class Pager:
    """Object allowing to make a pageable list"""

    def __init__(
        self,
        scene,
        gui_manager: GUIManager,
        tiles: list,
        position: tuple,
        grid: tuple,
        tile_dim: tuple,
        tile_margin: tuple,
    ):
        # Split tiles into chunks per page
        tiles_per_page = grid[0] * grid[1]
        tile_batches = [
            tiles[i : i + tiles_per_page] for i in range(0, len(tiles), tiles_per_page)
        ]

        self.scene = scene
        self.gui_manager = gui_manager

        self.pages = [
            Page(self, tile_batch, position, grid, tile_dim, tile_margin)
            for tile_batch in tile_batches
        ]

        self.page_number = 0
        self.setup_arrows()

    def setup(self):
        self.gui_manager.register(self.l_arrow, "foreground")
        self.gui_manager.register(self.r_arrow, "foreground")

        if self.pages:
            self.pages[0].register()
        else:
            self.scene.message_label.set_text("Cannot load map data from disk")

    def setup_arrows(self):
        self.l_arrow = Button(
            Rectangle(50, 50, 150, 150),
            pyglet.image.load("game/resources/gui/left_arrow.png"),
            pyglet.image.load("game/resources/gui/left_arrow.png"),
            pyglet.image.load("game/resources/gui/left_arrow.png"),
        )

        self.r_arrow = Button(
            Rectangle(1770, 50, 1870, 150),
            pyglet.image.load("game/resources/gui/right_arrow.png"),
            pyglet.image.load("game/resources/gui/right_arrow.png"),
            pyglet.image.load("game/resources/gui/right_arrow.png"),
        )

        self.l_arrow.on_lclick_pressed_clbck = self.page_left
        self.r_arrow.on_lclick_pressed_clbck = self.page_right

    def page_right(self, x, y, modifiers, obj):
        """Move page to right"""
        if self.page_number + 1 < len(self.pages):
            self.pages[self.page_number].unregister()
            self.page_number += 1
            self.pages[self.page_number].register()

            self.scene.window.on_resize(
                self.scene.window.width, self.scene.window.height
            )

    def page_left(self, x, y, modifiers, obj):
        """Move page to left"""
        if self.page_number > 0:
            self.pages[self.page_number].unregister()
            self.page_number -= 1
            self.pages[self.page_number].register()

            self.scene.window.on_resize(
                self.scene.window.width, self.scene.window.height
            )


class Page:
    """Single page object"""

    def __init__(
        self,
        pager,
        tiles_data: list,
        position: tuple,
        grid: tuple,
        tile_dim: tuple,
        tile_margin: tuple,
    ):
        self.pager = pager
        self.tiles = []

        row, col = 0, 0

        for tile in tiles_data:

            if col >= grid[0]:
                col = 0
                row += 1

            # Lower left corner of image
            p1 = (
                position[0] + (tile_dim[0] + tile_margin[0]) * col,
                position[1] - tile_dim[0] - (tile_dim[1] + tile_margin[1]) * row,
            )

            # Upper right corner of image
            p2 = (
                position[0] + tile_dim[0] + (tile_dim[0] + tile_margin[0]) * col,
                position[1] - (tile_dim[1] + tile_margin[1]) * row,
            )

            label = TextLabel(
                Rectangle(p1[0], p1[1] - 20, p2[0], p2[1] - tile_dim[1]),
                tile.name,
                valign="center",
                halign="center",
            )

            # Thumbnail button
            main_button = Button(
                Rectangle(*p1, *p2),
                tile.image,
                tile.image,
                tile.image,
            )

            # State button
            state_button = Button(
                Rectangle(p2[0] - 50, p2[1] - 50, *p2),
                tile.state.get_image(),
                tile.state.get_image(),
                tile.state.get_image(),
            )

            state_button.on_lclick_pressed_clbck = self.pager.scene.state_action(tile)
            main_button.on_lclick_pressed_clbck = self.pager.scene.play(
                tile, state_button
            )

            self.tiles.append((main_button, state_button, label))
            col += 1

    def register(self):
        """Register all page components"""
        for main_button, state_button, label in self.tiles:
            self.pager.gui_manager.register(main_button, "foreground")
            self.pager.gui_manager.register(state_button, "foreground")
            self.pager.gui_manager.register(label, "foreground")

    def unregister(self):
        """Unregister all page components"""
        for main_button, state_button, label in self.tiles:
            self.pager.gui_manager.unregister(main_button)
            self.pager.gui_manager.unregister(state_button)
            self.pager.gui_manager.unregister(label)
