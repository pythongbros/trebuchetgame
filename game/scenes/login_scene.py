import pyglet
from game.core.geometry import Rectangle
from game.gui import Button, InputBox, Icon

from game.connectors import ServerConnector
from .scene import Scene
from game.window import Window


class LoginScene(Scene):
    """This is a window representing the user login scene

    Attributes
    ----------

    """

    def __init__(self, window: Window, **kwargs):
        super().__init__(window)

        self.icon = Icon(
            Rectangle(621, 646, 1299, 1000),
            img=pyglet.image.load("game/resources/gui/logotitle.png"),
        )

        self.email_input = InputBox(
            Rectangle(810, 460, 1110, 510),
            "Wpisz email",
            image=pyglet.image.load("game/resources/gui/input_box.png"),
        )

        self.password_input = InputBox(
            Rectangle(810, 370, 1110, 420),
            "Wpisz hasło",
            image=pyglet.image.load("game/resources/gui/input_box.png"),
            hashed=True,
        )

        self.play_button = Button(
            Rectangle(910, 250, 1010, 300),
            pyglet.image.load("game/resources/gui/button_violet_def.png"),
            pyglet.image.load("game/resources/gui/button_violet_hov.png"),
            pyglet.image.load("game/resources/gui/button_violet_pre.png"),
            "Login",
        )

        self.play_button.on_lclick_pressed_clbck = self.validate

    def setup(self):
        super().setup()
        self.gui_manager.register(self.icon, "foreground")
        self.gui_manager.register(self.email_input, "foreground")
        self.gui_manager.register(self.password_input, "foreground")
        self.gui_manager.register(self.play_button, "foreground")

    def validate(self, x, y, modifiers, obj):
        from .menu_scene import MenuScene

        code, msg = ServerConnector.login(
            self.email_input.get_text(), self.password_input.get_text()
        )

        if code == 200:
            self.change_scene(MenuScene)
        else:
            self.message_label.set_text(msg)

    def update(self, dt: float):
        super().update(dt)
