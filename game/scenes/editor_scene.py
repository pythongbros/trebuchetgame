import _thread as thread
import math
import json
import os

import pyglet
import pymunk
from pyglet import image

import tkinter as tk
from tkinter import filedialog, simpledialog, messagebox

from game.core.geometry import Rectangle, Point
from game.utils.graphics import scale_image
from game.gui import Button, Icon, ProgressBar, ProjectileTray, GUIElement
from game.objects.templates.concrete.mapper import TemplateMapper
from game.objects.templates.concrete import SpaceshipTemplate
from game.objects import SceneObject, PhysicsObject, DestructableObject, Player, Mob

from .scene import Scene
from game.gui.editor import Toolbox
from game.window import Window
from game.system import ObjectManager
from .utils import Scroller


class EditorScene(Scene):
    """Editor scene"""

    def __init__(self, window: Window, **kwargs):
        super().__init__(window)

        space = pymunk.Space()
        self.obj_manager = ObjectManager(space, self.window.width, self.window.height)

        # Initialize ObjectManager
        self.obj_manager = ObjectManager(
            pymunk.Space(), self.window.original_dim[0], self.window.original_dim[1]
        )

        # Init default map properties:
        self.map_props = {
            "milk_max": 100,
            "img_background": "game/resources/gui/editor/img_back_default.png",
            "img_planet": "game/resources/gui/editor/img_planet_default.png",
            "spaceship": None,
            "gravity": (0, -1500),
        }

        self.scroller = Scroller(self.window.original_dim, 5)

        self.sector_spaceship = GUIElement(Rectangle(0, 100, 300, 1000))
        self.sector_building = GUIElement(Rectangle(800, 0, 1920, 1000))

        self.sector_spaceship.set_visibility(False)
        self.sector_spaceship.on_lclick_pressed_clbck = self.place_spaceship
        self.sector_building.set_visibility(False)
        self.sector_building.on_lclick_pressed_clbck = self.place

        self.active_sector = None
        self.simulation_on = False
        self.init_state = {}

        self.obj_manager.create_group("map")

        self.gui_manager.create_group("sectors")
        self.gui_manager.register(self.sector_spaceship, "sectors")
        self.gui_manager.register(self.sector_building, "sectors")

        self.gui_manager.on_mouse_motion_clbck = self.on_mouse_motion
        self.gui_manager.on_mouse_press_clbck = self.on_mouse_press
        self.gui_manager.on_mouse_scroll_clbck = self.on_mouse_scroll

        # Currently held element:
        self.holding = None
        self.holding_tmpl = None
        self.cursor_mode = "normal"

        # Mutex on file dialog window:
        self.dialog_busy = False

    def setup(self):
        super().setup()
        self.window.push_handlers(self.scroller.on_key_press)
        self.window.push_handlers(self.scroller.on_key_release)

    def on_draw(self):
        self.window.clear()
        translation = self.scroller.get_translation()
        self.gui_manager.draw_group("background")
        self.obj_manager.draw_all(translation)
        self.gui_manager.draw_group("foreground")

    def update(self, dt):
        self.scroller.update(self.window.width, self.window.height)
        self.obj_manager.update_all()

        if self.simulation_on:
            self.obj_manager.space.step(1.0 / 60)

        super().update(dt)

    def destructor(self):
        self.obj_manager.destruct()
        super().destructor

    def on_mouse_motion(self, x, y, dx, dy):
        if self.holding is not None:
            pos = Point(*self.gui_manager.mouse_pos)
            self.holding.set_position(pos.x, pos.y)

            if not self.active_sector.rectangle.is_point_inside(pos):
                if self.cursor_mode == "hand":
                    cursor = self.window.get_system_mouse_cursor(self.window.CURSOR_NO)
                    self.window.set_mouse_cursor(cursor)
                    self.cursor_mode = "no"
            else:
                if self.cursor_mode == "no":
                    cursor = self.window.get_system_mouse_cursor(
                        self.window.CURSOR_HAND
                    )
                    self.window.set_mouse_cursor(cursor)
                    self.cursor_mode = "hand"

    def on_mouse_press(self, x, y, button, modifiers):
        if button == pyglet.window.mouse.RIGHT:
            if self.holding is not None:
                self.cancel_hold()
            else:
                obj = self.obj_manager.get_object_at(x, y)
                if obj is not None:
                    if isinstance(obj, Player):
                        self.map_props["spaceship"] = None
                    else:
                        del self.init_state[obj]
                    self.obj_manager.unregister(obj)

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        pass
        # if self.holding is not None and not isinstance(self.holding, Player):
        #    self.holding.rotate(scroll_y * math.pi / 8)

    def choose_map_file(self, x, y, modifiers, obj):
        if not self.dialog_busy and not self.simulation_on:
            thread.start_new_thread(
                self.open_file_dialog,
                (
                    "Select map file to load",
                    (
                        ("All files", "*.*"),
                        ("JSON file", "*.json"),
                    ),
                    self.load_map,
                ),
            )

    def create_map_file(self, x, y, modifiers, obj):
        if (
            not self.dialog_busy
            and not self.simulation_on
            and self.map_props["spaceship"] is not None
        ):
            thread.start_new_thread(
                self.save_map_dialog,
            )

    def load_map(self, file_path):
        pass

    def save_map(self, dir_path, descr, name):
        data = {"physics_objects": [], "destructable_objects": [], "mobs": []}
        data["gravity"] = self.map_props["gravity"]
        data["player"] = {
            "template": "spaceship",
            "position": self.map_props["spaceship"].position,
        }
        data["projectiles"] = [
            {"template": projectile_data[1]}
            for projectile_data in self.projectile_tray.queue
        ]

        for obj, props in self.init_state.items():
            template_name = None
            for k, v in TemplateMapper.items():
                if v == props[0]:
                    template_name = k
                    break

            if isinstance(obj, Mob):
                data["mobs"].append(
                    {"template": template_name, "position": list(props[1])}
                )
            elif isinstance(obj, DestructableObject):
                data["destructable_objects"].append(
                    {"template": template_name, "position": list(props[1])}
                )
            elif isinstance(obj, PhysicsObject):
                data["physics_objects"].append(
                    {"template": template_name, "position": list(props[1])}
                )

        description = {"_id": "0", "name": name, "description": descr}

        dirname = "".join(x for x in name if x.isalnum())
        path = dir_path + "/map_" + dirname

        try:
            os.mkdir(path)
        except OSError as e:
            self.display_error_messagebox(
                "Cannot save map - exception occured:\n" + str(e)
            )
            return

        with open(path + "/object.json", "w+") as file:
            json.dump(data, file)

        with open(path + "/info.json", "w+") as file:
            json.dump(description, file)

    def choose_background_img(self, x, y, modifiers, obj):
        if not self.dialog_busy:
            thread.start_new_thread(
                self.open_file_dialog,
                (
                    "Select background image",
                    (
                        ("All files", "*.*"),
                        ("JPEG image", "*.jpe?g"),
                        ("PNG image", "*.png"),
                    ),
                    self.schedule_background_img_update,
                ),
            )

    def choose_planet_img(self, x, y, modifiers, obj):
        if not self.dialog_busy:
            thread.start_new_thread(
                self.open_file_dialog,
                (
                    "Select planet image",
                    (
                        ("All files", "*.*"),
                        ("JPEG image", "*.jpe?g"),
                        ("PNG image", "*.png"),
                    ),
                    self.schedule_planet_img_update,
                ),
            )

    def adjust_milk_level(self, x, y, modifiers, obj):
        if not self.dialog_busy:
            thread.start_new_thread(
                self.open_number_dialog,
                (
                    "Set milk level",
                    "Specify desired milk level (current: "
                    + str(self.map_props["milk_max"])
                    + "): ",
                    self.schedule_milk_update,
                ),
            )

    def adjust_gravity(self, x, y, modifiers, obj):
        if not self.dialog_busy:
            thread.start_new_thread(
                self.open_number_dialog,
                (
                    "Set gravity",
                    "Specify desired gravity level (current: "
                    + str(self.map_props["gravity"][1])
                    + "): ",
                    self.update_gravity,
                ),
            )

    def _build_gui(self):
        width, height = self.window.original_dim

        self.background = Icon(
            Rectangle(0, 0, width, height),
            pyglet.image.load("game/resources/gui/background.png"),
        )
        self.gui_manager.register(self.background, "background")

        self.tray_upper = Icon(
            Rectangle(0, height - 80, width, height),
            pyglet.image.load("game/resources/gui/editor/tray_upper.png"),
        )
        self.gui_manager.register(self.tray_upper, "foreground")

        self.tray_lower = Icon(
            Rectangle(0, 0, 500, 100),
            pyglet.image.load("game/resources/gui/editor/tray_lower.png"),
        )
        self.gui_manager.register(self.tray_lower, "foreground")

        ltray_offset_x = 20
        ltray_offset_y = 15
        button_size = 64

        button_ltray_props = [
            (
                "game/resources/gui/editor/button_ufo.png",
                self.hold_spaceship,
                (SpaceshipTemplate, Player),
            ),
            (
                "game/resources/gui/editor/button_back.png",
                self.choose_background_img,
                None,
            ),
            (
                "game/resources/gui/editor/button_planet.png",
                self.choose_planet_img,
                None,
            ),
            ("game/resources/gui/editor/button_milk.png", self.adjust_milk_level, None),
            ("game/resources/gui/editor/button_gravity.png", self.adjust_gravity, None),
        ]
        self.button_ltray = []

        for i in range(len(button_ltray_props)):
            img = pyglet.image.load(button_ltray_props[i][0])

            button = Button(
                Rectangle(
                    ltray_offset_x * (i + 1) + button_size * i,
                    ltray_offset_y,
                    (ltray_offset_x + button_size) * (i + 1),
                    ltray_offset_y + button_size,
                ),
                img,
                img,
                img,
            )
            button.on_lclick_pressed_clbck = button_ltray_props[i][1]
            button.data = button_ltray_props[i][2]
            self.tray_upper.add_child(button)

        button_rtray_props = [
            ("game/resources/gui/editor/button_play.png", self.toggle_simulation, None),
            ("game/resources/gui/editor/button_save.png", self.create_map_file, None),
            ("game/resources/gui/editor/button_load.png", self.choose_map_file, None),
        ]
        self.button_rtray = []

        for i in range(len(button_rtray_props)):
            img = pyglet.image.load(button_rtray_props[i][0])

            button = Button(
                Rectangle(
                    width - ((ltray_offset_x + button_size) * (i + 1)),
                    ltray_offset_y,
                    width - (ltray_offset_x * (i + 1) + button_size * i),
                    ltray_offset_y + button_size,
                ),
                img,
                img,
                img,
            )

            button.on_lclick_pressed_clbck = button_rtray_props[i][1]
            button.data = button_rtray_props[i][2]
            self.tray_upper.add_child(button)

        self.milk_bar = ProgressBar(
            Rectangle(50, 65, 360, 80),
            pyglet.image.load("game/resources/gui/editor/bar_milk_frame.png"),
            pyglet.image.load("game/resources/gui/editor/bar_milk_fill.png"),
            self.map_props["milk_max"],
            self.map_props["milk_max"],
            50,
            0,
        )
        self.tray_lower.add_child(self.milk_bar)

        self.projectile_tray = ProjectileTray(Rectangle(10, 10, 460, 50), 8, True)
        self.gui_manager.register(self.projectile_tray.context_menu, "foreground")

        self.tray_lower.add_child(self.projectile_tray)

        self.toolbox = Toolbox(Rectangle(750, 25, 1080, 67), 5, self.hold)
        self.tray_upper.add_child(self.toolbox)

    def _build_scene(self):
        planet_offset = -250
        planet_img = scale_image(
            pyglet.image.load(self.map_props["img_planet"]), 1400, 200
        )
        self.planet = SceneObject(planet_img, (0, 0))
        self.planet.permament = True
        self.planet.set_position(
            self.window.original_dim[0] + planet_offset,
            self.planet.image.height / 2,
        )

        self.obj_manager.register(self.planet)

    def open_file_dialog(self, title, filetypes, function):
        self.dialog_busy = True

        tk_root = tk.Tk()
        tk_root.withdraw()

        file_path = filedialog.askopenfilename(
            title=title,
            filetypes=filetypes,
        )

        tk_root.destroy()

        if file_path:
            function(file_path)

        self.dialog_busy = False
        return

    def save_map_dialog(self):
        self.dialog_busy = True

        tk_root = tk.Tk()
        tk_root.withdraw()

        name = tk.simpledialog.askstring(
            title="Map name", prompt="Please enter map name:"
        )

        if name is None:
            tk_root.destroy()
            self.dialog_busy = False
            return

        descr = tk.simpledialog.askstring(
            title="Map description", prompt="Please enter short description:"
        )

        if descr is None:
            tk_root.destroy()
            self.dialog_busy = False
            return

        dir_path = filedialog.askdirectory(title="Choose directory to save map to")

        tk_root.destroy()

        if dir_path:
            self.save_map(dir_path, descr, name)

        self.dialog_busy = False
        return

    def open_number_dialog(self, title, prompt, function):
        self.dialog_busy = True

        tk_root = tk.Tk()
        tk_root.withdraw()

        number = tk.simpledialog.askinteger(title=title, prompt=prompt)

        tk_root.destroy()

        if number:
            function(number)

        self.dialog_busy = False
        return

    def display_error_messagebox(self, message):
        tk_root = tk.Tk()
        tk_root.withdraw()

        messagebox.showerror("Error", message)

        tk_root.destroy()

    def schedule_background_img_update(self, img_path):
        self.map_props["img_background"] = img_path
        self.background_update = True
        pyglet.clock.schedule(self.update_background_image)

    def schedule_planet_img_update(self, img_path):
        self.map_props["img_planet"] = img_path
        self.planet_update = True
        pyglet.clock.schedule(self.update_planet_image)

    def schedule_milk_update(self, milk):
        self.map_props["milk_max"] = milk
        self.milk_update = True
        pyglet.clock.schedule(self.update_max_milk)

    def update_gravity(self, gravity):
        self.map_props["gravity"] = 0, gravity

    def update_background_image(self, dt):
        if hasattr(self, "background_update") and self.background_update:
            self.background.set_image(
                pyglet.image.load(self.map_props["img_background"])
            )

            del self.background_update
            pyglet.clock.unschedule(self.update_planet_image)

    def update_planet_image(self, dt):
        if hasattr(self, "planet_update") and self.planet_update:
            img = pyglet.image.load(self.map_props["img_planet"])
            img.anchor_x = img.width // 2
            img.anchor_y = img.height // 2
            self.planet.image = img

            del self.planet_update
            pyglet.clock.unschedule(self.update_planet_image)

    def update_max_milk(self, milk):
        if hasattr(self, "milk_update") and self.milk_update:
            self.milk_bar.max_value = milk
            self.milk_bar.set_progress(milk)

            del self.milk_update
            pyglet.clock.unschedule(self.update_max_milk)

    def hold(self, data):
        if self.holding is not None:
            self.cancel_hold()

        if self.simulation_on:
            return

        self.sector_building.set_visibility(True)
        self.active_sector = self.sector_building

        cursor = self.window.get_system_mouse_cursor(self.window.CURSOR_HAND)
        self.window.set_mouse_cursor(cursor)
        self.cursor_mode = "hand"

        obj = data[1].from_template(data[0](self.gui_manager.mouse_pos))
        obj.permament = True

        self.obj_manager.register(obj, "map")
        self.holding = obj
        self.holding_tmpl = data[0]

    def cancel_hold(self):
        self.obj_manager.unregister(self.holding)
        self.holding = None
        self.holding_tmpl = None
        self.active_sector = None

        self.sector_spaceship.set_visibility(False)
        self.sector_building.set_visibility(False)

        cursor = self.window.get_system_mouse_cursor(self.window.CURSOR_DEFAULT)
        self.window.set_mouse_cursor(cursor)
        self.cursor_mode = "normal"

    def place(self, x, y, modifiers, obj):
        if self.cursor_mode == "no":
            return

        pos = self.holding.shape.body.position
        rot = self.holding.shape.body.angle

        if self.holding.shape.body.body_type == pymunk.Body.STATIC:
            # Need to replace that object with a new one
            self.obj_manager.unregister(self.holding)
            self.holding = type(self.holding).from_template(self.holding_tmpl(pos))
            self.holding.shape.body.position = pos
            self.holding.shape.body.angle = rot
            self.obj_manager.register(self.holding, "map")

        self.init_state[self.holding] = (
            self.holding_tmpl,
            pos,
            rot,
        )

        self.holding = None
        self.holding_tmpl = None
        self.sector_building.set_visibility(False)
        self.active_sector = None

        cursor = self.window.get_system_mouse_cursor(self.window.CURSOR_DEFAULT)
        self.window.set_mouse_cursor(cursor)
        self.cursor_mode = "normal"

    def hold_spaceship(self, x, y, modifiers, obj):
        data = obj.data

        if self.holding is not None:
            self.cancel_hold()

        if self.simulation_on:
            return

        self.sector_spaceship.set_visibility(True)
        self.active_sector = self.sector_spaceship

        cursor = self.window.get_system_mouse_cursor(self.window.CURSOR_HAND)
        self.window.set_mouse_cursor(cursor)
        self.cursor_mode = "hand"

        obj = data[1].from_template(data[0](self.gui_manager.mouse_pos))
        obj.permament = True
        self.obj_manager.register(obj)
        self.holding = obj
        self.holding_tmpl = data[0]

    def place_spaceship(self, x, y, modifiers, obj):
        if self.cursor_mode == "no":
            return

        if self.map_props["spaceship"] is not None:
            self.obj_manager.unregister(self.map_props["spaceship"])

        self.map_props["spaceship"] = self.holding

        self.holding = None
        self.holding_tmpl = None
        self.sector_spaceship.set_visibility(False)
        self.active_sector = None

        cursor = self.window.get_system_mouse_cursor(self.window.CURSOR_DEFAULT)
        self.window.set_mouse_cursor(cursor)
        self.cursor_mode = "normal"

    def toggle_simulation(self, x, y, modifiers, obj):
        if self.holding is not None:
            self.cancel_hold()

        if not self.simulation_on:
            img = pyglet.image.load("game/resources/gui/editor/button_stop.png")
            self.start_simulation()
        else:
            img = pyglet.image.load("game/resources/gui/editor/button_play.png")
            self.reset_simulation()

        obj.img_default = img
        obj.img_clicked = img
        obj.img_hovered = img

    def start_simulation(self):
        self.obj_manager.space.gravity = self.map_props["gravity"]
        self.simulation_on = True

    def reset_simulation(self):
        self.obj_manager.unregister_all_from("map")
        state_copy = {}

        for key, val in self.init_state.items():
            obj = type(key).from_template(val[0](val[1]))
            obj.shape.body.position = val[1]
            obj.shape.body.angle = val[2]
            obj.permament = True

            state_copy[obj] = val
            self.obj_manager.register(obj, "map")

        self.init_state = state_copy
        self.obj_manager.space.gravity = (0, 0)
        self.simulation_on = False
