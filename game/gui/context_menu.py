import pyglet
from pyglet import image

from game.core.geometry import Rectangle
from .button import Button
from .gui_element import GUIElement


class ContextMenu(GUIElement):
    """A class used for creation of simple context menu.

    Attrubites
    ----------
    options : list
        The list of all options
    """

    def __init__(
        self,
        position: tuple,
        options: list,
        img_path: str = "game/resources/gui/context_menu.png",
    ):
        """Initializator builds context menu"""
        img = pyglet.image.load(img_path)
        field_width, field_height = img.width, img.height
        super().__init__(
            Rectangle(
                position[0],
                position[1],
                position[0] + field_width,
                position[1] + field_height,
            )
        )
        self.options = []

        for i in range(len(options)):
            button = Button(
                Rectangle(0, i * field_height, field_width, (i + 1) * field_height),
                img,
                img,
                img,
                options[i],
                color=(0, 0, 0, 255),
            )
            self.options.append(button)
            self.add_child(button)

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)

        # Finally makes all of it's children resize
        for child in self.children:
            child._rescale(dx, dy)

    def draw(self):
        """Draws the element on the screen"""
        pass
