from typing import Tuple
from abc import abstractmethod

from game.core.geometry import Rectangle, Point


class GUIElement:
    """This is the parent class for all GUI Elements.
    You should inherit from this class rather than
    create it's instances in order to define new
    GUI Elements; you may think of GUIElement as
    an interface.

    NOTE:
    Whenever you see a method which name begins
    with an underscore, that means it was
    created for an internal use within the class
    and probably should not called outside.
    """

    def __init__(self, rect: Rectangle):
        """Initializator expects a rectangle used as a Bounding Box for this GUIElement

        Attributes
        ----------

        rectangle : Rectangle
            Rectangle to specify the Bounding Box of this element

        on_lclick_clbck_pressed : Callable
            Callback function called on mouse left click pressed
        on_rclick_clbck_pressed : Callable
            Callback function called on mouse right click pressed
        on_lclick_clbck_released : Callable
            Callback function called on mouse left click released
        on_rclick_clbck_released : Callable
            Callback function called on mouse right click released
        on_hover_begin_clbck : Callable
            Callback function called on mouse hover begin
        on_hover_clbck : Callable
            Callback function called on mouse hover
        on_hover_end_clbck : Callable
            Callback function called on mouse hover end

        is_hovered : bool
            True if element is being hovered by mouse cursor
        children : list
            List of this element's children.
        is_visible : bool
            True if the element should be drawn (also affects it's children)
        parent : GUIElement
            Parent of this element

        _origin_rect : Rectangle
            The rectangle of this object on the original canvas
            (see GUIManager._origin_screen_size)
        """

        # Create a Bounding Box for this GUI Element:
        self.rectangle = rect
        self._origin_rect = rect

        # Every GUI Element has a set of common callbacks;
        # the GUIManager class is responsible for
        # executing them when it's time to.

        # This callback is executed when the left mouse click
        # was performed on this element
        self.on_lclick_pressed_clbck = lambda *_: None

        # This callback is executed when the right mouse click
        # was performed on this element
        self.on_rclick_pressed_clbck = lambda *_: None

        # This callback is executed when the left mouse button
        # was released on this element
        self.on_lclick_released_clbck = lambda *_: None

        # This callback is executed when the right mouse click
        # was released on this element
        self.on_rclick_released_clbck = lambda *_: None

        # This callback is executed when the mouse cursor begins hovering
        # above this element
        self.on_hover_begin_clbck = lambda *_: None

        # This callback is executed when the mouse cursor hovers
        # above this element
        self.on_hover_clbck = lambda *_: None

        # This callback is executed when the mouse cursor ends hovering
        # above this element
        self.on_hover_end_clbck = lambda *_: None

        # This callback is executed when the mouse wheel was scrolled
        self.on_mouse_scroll_clbck = lambda *_: None

        # Init hovering status
        self.is_hovered = False

        # Init empty children list:
        self.children = []

        # Init visibility:
        self.is_visible = True

        # Init parent:
        self.parent = None

        # A set of internal callbacks, that are common
        # for all GUI Elements; called by GUIManager:

    def on_hover_begin(self, x: int, y: int, dx: int, dy: int):
        """On mouse hover begin callback"""
        self.is_hovered = True
        self.on_hover_begin_clbck(x, y, dx, dy, self)

    def on_hover_end(self, x: int, y: int, dx: int, dy: int):
        """On mouse hover end callback"""
        self.is_hovered = False
        self.on_hover_end_clbck(x, y, dx, dy, self)

    def on_hover(self, x: int, y: int, dx: int, dy: int):
        """On mouse hover callback"""
        self.on_hover_clbck(x, y, dx, dy, self)

    def handle_mouse_motion(self, x: int, y: int, dx: int, dy: int):
        """Handles mouse hovering over element"""
        if self.rectangle.is_point_inside(Point(x, y)):
            if not self.is_hovered:
                self.on_hover_begin(x, y, dx, dy)
            else:
                self.on_hover(x, y, dx, dy)
        elif self.is_hovered:
            self.on_hover_end(x, y, dx, dy)

        for child in self.children:
            if child.is_visible:
                child.handle_mouse_motion(x, y, dx, dy)

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        """On mouse wheel scroll callback"""
        self.on_mouse_scroll_clbck(x, y, scroll_x, scroll_y)

        for child in self.children:
            if child.get_absolute_rect().is_point_inside(Point(x, y)):
                child.on_mouse_scroll_clbck(x, y, scroll_x, scroll_y)

    def on_lclick_pressed(self, x: int, y: int, modifiers):
        """On left mouse click pressed callback"""
        self.on_lclick_pressed_clbck(x, y, modifiers, self)

        for child in self.children:
            if child.get_absolute_rect().is_point_inside(Point(x, y)):
                child.on_lclick_pressed(x, y, modifiers)

    def on_rclick_pressed(self, x: int, y: int, modifiers):
        """On right mouse click callback"""
        self.on_rclick_pressed_clbck(x, y, modifiers, self)

        for child in self.children:
            if child.get_absolute_rect().is_point_inside(Point(x, y)):
                child.on_rclick_pressed(x, y, modifiers)

    def on_lclick_released(self, x: int, y: int, modifiers):
        """On left mouse click released callback"""
        self.on_lclick_released_clbck(x, y, modifiers, self)

        for child in self.children:
            if child.get_absolute_rect().is_point_inside(Point(x, y)):
                child.on_lclick_released(x, y, modifiers)

    def on_rclick_released(self, x: int, y: int, modifiers):
        """On right mouse click released callback"""
        self.on_rclick_released_clbck(x, y, modifiers, self)

        for child in self.children:
            if child.get_absolute_rect().is_point_inside(Point(x, y)):
                child.on_rclick_released(x, y, modifiers)

    def on_key_press(self, symbol, modifiers):
        """On key press callback"""
        for child in self.children:
            child.on_key_press(symbol, modifiers)

    def on_key_release(self, symbol, modifiers):
        """On key release callback"""
        for child in self.children:
            child.on_key_release(symbol, modifiers)

    def on_text(self, text):
        """On text input callback"""
        for child in self.children:
            child.on_text(text)

    def on_focus(self, focus: bool):
        """On focus change callback"""
        for child in self.children:
            child.on_focus(focus)

    def add_child(self, child):
        """Adds a new child to this element"""
        """ IMPORTANT: Always prevent looping child-parent relations!!!"""

        if child not in self.children and self not in child.children:
            child.parent = self
            self.children.append(child)

    def remove_child(self, child):
        """Removes given element from children list"""
        if child in self.children:
            child.parent = None
            self.children.remove(child)

    def set_rect(self, rect: Rectangle):
        """Sets the new size of GUIElement"""
        dx = self.rectangle.get_width() / rect.get_width()
        dy = self.rectangle.get_height() / rect.get_height()

        self.rectangle = rect
        self._rescale(dx, dy)

    def get_absolute_rect(self):
        """Gets the absolute position of the element on the screen"""
        if self.parent is None:
            return self.rectangle
        else:
            m = self.rectangle
            p = self.parent.get_absolute_rect()
            return Rectangle(
                m.lower_left.x + p.lower_left.x,
                m.lower_left.y + p.lower_left.y,
                m.upper_right.x + p.lower_left.x,
                m.upper_right.y + p.lower_left.y,
            )

    def set_visibility(self, visible: bool):
        """Sets the visibility of GUIElement and it's children"""
        self.is_visible = visible

        for child in self.children:
            child.is_visible = visible

    def update(self):
        """Called every frame, defines the behaviour of this element"""
        pass

    def update_all(self):
        """Called every frame, realizes the behaviour of this element and all of it's children"""
        self.update()

        for child in self.children:
            child.update()

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)

        # Finally makes all of it's children resize
        for child in self.children:
            child._rescale(dx, dy)

    @abstractmethod
    def draw(self):
        """This method draws the GUI Element"""
        raise NotImplementedError

    def draw_all(self):
        """This method draws the GUI Element and all of it's children"""
        self.draw()

        for child in self.children:
            if child.is_visible:
                child.draw_all()
