from typing import Tuple

import pyglet
from pyglet.image import ImageData
from pyglet import clock
from pyglet.window import key

from game.utils.fonts import pts_to_pixel
from game.core.geometry import Rectangle
from .text_label import TextLabel

from game.utils.graphics import scale_image


class InputBox(TextLabel):
    """This is a GUI element that provides single line text input.

    Attributes
    ----------
    hashed : bool
        True if entered text should be hidden (e.g. for a password InputBox)
    real_text : str
        Stores the actual text in the box; used if hashed = True

    caret : int
        Position of the caret in the text
    _caret_height : int
        Height of the caret on the screen (relative)
    _active : bool
        True if text edit mode is active
    _caret_visibility : bool
        True if caret is visible
    _pressed_key_multiaction : Callable
        Stores the function to call repeatedly for key that was pressed but not released yet
    _multiaction_delay : float
        The delay in seconds from the keypress to the multiaction call
    _multiaction_press_time : float
        The time (seconds) the multiaction key is being pressed
    """

    def __init__(
        self,
        rect: Rectangle,
        text: str,
        image: ImageData = None,
        font_name: str = "Orbitron-Black",
        font_size: int = 12,
        color: Tuple[int] = (200, 200, 200, 255),
        halign: str = "center",
        valign: str = "center",
        anchor_x: str = "left",
        anchor_y: str = "bottom",
        hashed: bool = False,
        max_length: int = None,
    ):
        """Initializator creates an InputBox with given parameters and initial text"""
        super().__init__(
            rect,
            text,
            font_name,
            font_size,
            color,
            halign,
            valign,
            anchor_x,
            anchor_y,
            multiline=False,
        )

        self.image = image

        if self.image is not None:
            self.img_current = scale_image(
                self.image, rect.get_width(), rect.get_height()
            )
        else:
            self.img_current = None

        self._active = False
        self._caret_visibility = False

        self._pressed_key_multiaction = None
        self._multiaction_delay = 0.5
        self._multiaction_press_time = 0.0

        self.real_text = text

        self.caret = 0
        self._caret_height = pts_to_pixel(self.label.font_size)
        self.hashed = hashed
        self.max_length = max_length

        pyglet.clock.schedule_interval(self._caret_blink, 0.5)
        pyglet.clock.schedule_interval(self._multiaction, 0.05)

    def set_text(self, text):
        """Sets text of the InputBox"""
        self.real_text = text

        if self.hashed:
            super().set_text("•" * len(text))
        else:
            super().set_text(text)

    def get_text(self):
        """Gets the real text of the InputBox"""
        return self.real_text

    def set_font_size(self, font_size: int):
        """Sets font size for the text"""
        self.label.font_size = font_size
        self._caret_height = pts_to_pixel(self.label.font_size)

    def on_text(self, text):
        """On text input callback"""
        if self.max_length is not None and self.max_length < len(self.real_text + text):
            return

        # Handle only text here:
        self.set_text(
            self.get_text()[0 : self.caret] + text + self.get_text()[self.caret :]
        )
        self.caret += len(text)

        super().on_text(text)

    def _multiaction(self, dt):
        """Action performed when key was pressed for a long time"""
        if self._pressed_key_multiaction is not None:
            self._multiaction_press_time += dt

        if self._multiaction_press_time > self._multiaction_delay:
            self._pressed_key_multiaction()

    def _delete(self):
        """Action when delete pressed"""
        self.set_text(
            self.get_text()[0 : self.caret] + self.get_text()[self.caret + 1 :]
        )

    def _backspace(self):
        """Action when backspace pressed"""
        new_caret = max(self.caret - 1, 0)

        self.set_text(self.get_text()[0:new_caret] + self.get_text()[self.caret :])
        self.caret = new_caret

    def _move_caret_left(self):
        """Action to move the caret left"""
        self.caret = max(self.caret - 1, 0)

    def _move_caret_right(self):
        """Action to move the caret right"""
        self.caret = min(self.caret + 1, len(self.get_text()))

    def on_key_press(self, symbol, modifiers):
        """On key press callback"""
        # Handle only special keys here:
        if symbol == key.BACKSPACE:
            self._pressed_key_multiaction = self._backspace
            self._multiaction_press_time = 0.0
            self._backspace()
        elif symbol == key.DELETE:
            self._pressed_key_multiaction = self._delete
            self._multiaction_press_time = 0.0
            self._delete()
        elif symbol == key.HOME:
            self.caret = 0
        elif symbol == key.LEFT:
            self._pressed_key_multiaction = self._move_caret_left
            self._multiaction_press_time = 0.0
            self._move_caret_left()
        elif symbol == key.RIGHT:
            self._pressed_key_multiaction = self._move_caret_right
            self._multiaction_press_time = 0.0
            self._move_caret_right()
        elif symbol == key.END:
            self.caret = len(self.get_text())

        super().on_key_press(symbol, modifiers)

    def on_key_release(self, symbol, modifiers):
        """On key released callback"""
        self._pressed_key_multiaction = None
        self._multiaction_press_time = 0.0
        super().on_key_release(symbol, modifiers)

    def _caret_blink(self, dt):
        """This function performs the caret blinking effect"""
        if self._active:
            self._caret_visibility = not self._caret_visibility

    def on_focus(self, focus: bool):
        """On focus change callback"""
        if not focus:
            self._active = False
            self._caret_visibility = False
        else:
            self._active = True
            self._caret_visibility = True
            self.caret = len(self.get_text())

        super().on_focus(focus)

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)

        if self.img_current is not None:
            self.img_current = scale_image(
                self.image,
                self.rectangle.get_width(),
                self.rectangle.get_height(),
            )

    def draw(self):
        """Draws the element"""
        if self.img_current is not None:
            self.img_current.blit(
                self.get_absolute_rect().lower_left.x,
                self.get_absolute_rect().lower_left.y,
                0,
            )

        super().draw()

        if self._caret_visibility:
            temp = self.get_text()
            self.set_text(temp[0 : self.caret])

            if self.caret == 0:
                offset_x = 0
            else:
                offset_x = self.label.content_width

            center = self.get_absolute_rect().get_center()

            pyglet.graphics.draw(
                2,
                pyglet.gl.GL_LINES,
                (
                    "v2f",
                    (
                        center.x + offset_x / 2,
                        center.y - self._caret_height / 2,
                        center.x + offset_x / 2,
                        center.y + self._caret_height / 2,
                    ),
                ),
            )

            self.set_text(temp)
