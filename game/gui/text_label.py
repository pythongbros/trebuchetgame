from typing import Tuple

from pyglet.text import Label

from .gui_element import GUIElement
from game.core.geometry import Rectangle


class TextLabel(GUIElement):
    """This is a class representing text label

    Attributes
    ----------
    label : Label
        Pyglet label used to display text
    _origin_text : str
        Original text of the label
    """

    def __init__(
        self,
        rect: Rectangle,
        text: str,
        font_name: str = "Orbitron-Black",
        font_size: int = 12,
        color: Tuple[int] = (200, 200, 200, 255),
        halign: str = "left",
        valign: str = "top",
        anchor_x: str = "left",
        anchor_y: str = "baseline",
        multiline: bool = False,
    ):
        """Initializator creates label from given rectangle and sets it's text"""

        super().__init__(rect)
        self._origin_text = text
        self.halign = halign

        self.label = Label(
            text,
            font_name=font_name,
            font_size=font_size,
            x=rect.lower_left.x,
            y=rect.lower_left.y,
            width=rect.get_width(),
            height=rect.get_height(),
            color=color,
            anchor_x=anchor_x,
            anchor_y=anchor_y,
            multiline=True,
        )

        self.label.content_valign = valign
        self.label.multiline = multiline

    def set_text(self, text: str):
        """Sets text of the label"""
        self._origin_text = text
        self.label.text = text
        self._fit_text()

    def get_text(self):
        """Returns displayed text"""
        return self.label.text

    def set_color(self, color: Tuple[int]):
        """Sets color of the text"""
        self.label.color = color

    def get_color(self):
        """Returns displayed text's color"""
        return self.label.color

    def set_font(self, font_name: str):
        """Sets font for the text"""
        self.label.font_name = font_name

    def get_font(self):
        """Returns displayed text's font"""
        return self.label.font_name

    def set_font_size(self, font_size: int):
        """Sets font size for the text"""
        self.label.font_size = font_size

    def get_font_size(self):
        """Returns displayed text's font size"""
        return self.label.font_size

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)

        # Trim the text:
        self._fit_text()

        # Finally makes all of it's children resize
        for child in self.children:
            child._rescale(dx, dy)

    def _fit_text(self):
        """Trims the text to fit the rectangle"""
        self.label.text = self._origin_text
        iterator = len(self.label.text)

        if self.label.multiline:
            while (
                self.label.content_height > self.rectangle.get_height() and iterator > 0
            ):
                iterator -= 1
                self.label.text = self._origin_text[0:iterator] + "..."
        else:
            while (
                self.label.content_width > self.rectangle.get_width() and iterator > 0
            ):
                iterator -= 1
                self.label.text = self._origin_text[0:iterator] + "..."

    def draw(self):
        """Draws the element"""

        rect = self.get_absolute_rect()

        offset = 0
        if self.halign == "center":
            offset = (rect.get_width() - self.label.content_width) // 2
        elif self.halign == "right":
            offset = rect.get_width() - self.label.content_width

        # Update the position of this element:
        self.label.x = rect.lower_left.x + offset
        self.label.y = rect.lower_left.y
        self.label.width = rect.get_width()
        self.label.height = rect.get_height()

        self.label.draw()
