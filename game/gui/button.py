from pyglet.image import ImageData

from game.gui.gui_element import GUIElement
from game.gui.text_label import TextLabel
from game.core.geometry import Rectangle
from game.utils.graphics import scale_image


class Button(GUIElement):
    """GUI Button class

    Every button has 3 states: default, pressed and hovered.
    It's image is changed every time the state changes.
    To invoke some actions when pressed or hovered,
    use callback methods.

    Attributes
    ----------
    img_default : ImageData
        Default image
    img_hovered : ImageData
        Image set when hovered
    img_clicked : ImageData
        Image set when clicked
    img_current : ImageData
        Image currently displayed (scaled)
    text : TextLabel
        Text rendered on the button
    data : Object
        Some user data that can be stored in the button
    """

    def __init__(
        self,
        rect: Rectangle,
        img_default: ImageData,
        img_clicked: ImageData,
        img_hovered: ImageData,
        text: str = None,
        text_font: str = "Orbitron-Black",
        text_size: int = 12,
        color=(200, 200, 200, 255),
    ):
        """Initializator function creates button basing on given rectangle BBox"""
        super().__init__(rect)
        self.data = None

        self.img_default = img_default
        self.img_clicked = img_clicked
        self.img_hovered = img_hovered

        self.img_current = scale_image(
            self.img_default, rect.get_width(), rect.get_height()
        )

        if text is not None:
            self.text = TextLabel(
                Rectangle(
                    0, 0, self.rectangle.get_width(), self.rectangle.get_height()
                ),
                text,
                text_font,
                text_size,
                anchor_x="left",
                anchor_y="bottom",
                halign="center",
                valign="center",
                color=color,
            )

            self.add_child(self.text)
        else:
            self.text = None

    def on_hover_begin(self, x: int, y: int, dx: int, dy: int):
        """On mouse hover begin callback"""
        self.img_current = scale_image(
            self.img_hovered, self.rectangle.get_width(), self.rectangle.get_height()
        )

        super().on_hover_begin(x, y, dx, dy)

    def on_hover_end(self, x: int, y: int, dx: int, dy: int):
        """On mouse hover end callback"""
        self.img_current = scale_image(
            self.img_default, self.rectangle.get_width(), self.rectangle.get_height()
        )

        super().on_hover_end(x, y, dx, dy)

    def on_lclick_pressed(self, x: int, y: int, modifiers):
        """On left mouse click pressed callback"""
        self.img_current = scale_image(
            self.img_clicked, self.rectangle.get_width(), self.rectangle.get_height()
        )

        super().on_lclick_pressed(x, y, modifiers)

    def on_lclick_released(self, x: int, y: int, modifiers):
        """On left mouse click released callback"""
        if self.is_hovered:
            self.img_current = scale_image(
                self.img_hovered,
                self.rectangle.get_width(),
                self.rectangle.get_height(),
            )
        else:
            self.img_current = scale_image(
                self.img_default,
                self.rectangle.get_width(),
                self.rectangle.get_height(),
            )

        super().on_lclick_released(x, y, modifiers)

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)
        self.img_current = scale_image(
            self.img_default, self.rectangle.get_width(), self.rectangle.get_height()
        )

        # Finally makes all of it's children resize
        for child in self.children:
            child._rescale(dx, dy)

    def draw(self):
        """This method draws the GUI Element"""
        self.img_current.blit(
            self.get_absolute_rect().lower_left.x,
            self.get_absolute_rect().lower_left.y,
            0,
        )

        if self.text is not None:
            self.text.draw()
