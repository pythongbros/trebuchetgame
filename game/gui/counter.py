from typing import Union

from game.core.geometry import Rectangle
from .text_label import TextLabel


class Counter(TextLabel):
    """This class represents a simple Counter box

    This component can be used to count various things,
    like for example points or items. But if your intention
    is rather to count time, see the Timer class.

    Attributes
    ----------
    value : float
        Value of the counter
    decimals : int
        Set the counter precision
    step : float
        Default step to increment/decrement the counter
    """

    def __init__(
        self,
        rect: Rectangle,
        init_value: float = 0.0,
        decimals: int = 0,
        step: float = 1.0,
    ):
        """Initializator creates Counter using given parameters"""
        self.decimals = decimals
        self.step = step

        super().__init__(rect, "", halign="center", valign="center")
        self.set_value(init_value)

    def set_text(self, text: str):
        raise NotImplementedError(
            "Cannot set text of a Counter: use set_value() instead!"
        )

    def set_value(self, value: float):
        """Sets counter to a given value"""
        self.value = value
        super().set_text(
            ("{:." + str(self.decimals) + "f}").format(round(self.value, self.decimals))
        )

    def increment(self):
        """Increment the counted value by one step"""
        self.set_value(self.value + self.step)

    def decrement(self):
        """Decrement the counted value by one step"""
        self.set_value(self.value - self.step)

    def set_step(self, step):
        """Set the value of one increment/decrement step"""
        self.step = step

    def add(self, value: float):
        """Add given value to the counter"""
        self.set_value(self.value + value)

    def subtract(self, value: float):
        """Subtract given value from the counter"""
        self.set_value(self.value - value)
