from typing import Tuple
import copy

import pyglet
from pyglet import image

from game.core.geometry import Rectangle
from game.objects import Projectile
from game.objects.templates.concrete.mapper import TemplateMapper
from game.objects.templates.concrete import (
    ProjectileTemplate,
    LargeProjectileTemplate,
    AcceleratingProjectileTemplate,
    ZeroGravityProjectileTemplate,
    TeleportingProjectileTemplate,
)
from .button import Button
from .text_label import TextLabel
from .context_menu import ContextMenu
from .gui_element import GUIElement


class ProjectileTray(GUIElement):
    """A class used for visualization of a projectile queue.

    Remember to register also context_menu if you want to use it!

    Attrubites
    ----------
    queue : list
        The list of all stored projectiles
    tray : list
        List of all visual subcomponents
    mutable : bool
        True if user should be able to modify the list
    display_capacity : int
        Indicates how many projectiles should be visible at once
    _buttons_orig_dim : Tuple[int]
        Original dimension of each button
    """

    def __init__(self, rect: Rectangle, display_capacity: int, mutable: bool):
        """Initializator builds the projectiles queue tray"""
        super().__init__(rect)
        self.queue = []
        self.tray = []
        self.display_capacity = display_capacity
        self.mutable = mutable
        self._build_context_menu()
        self._buttons_orig_dim = rect.get_height(), rect.get_height()
        self._build_buttons()

    def _build_context_menu(self):
        """Constructs the context menu"""
        opts = {
            "Standard": ProjectileTemplate,
            "Teleporting": TeleportingProjectileTemplate,
            "Accelerating": AcceleratingProjectileTemplate,
            "Heavy": LargeProjectileTemplate,
            "Zero gravity": ZeroGravityProjectileTemplate,
        }
        pos = self.rectangle.lower_left.x, self.rectangle.upper_right.y

        self.context_menu = ContextMenu(pos, list(opts.keys()))
        self.context_menu.set_visibility(False)
        dim = self.rectangle.get_height(), self.rectangle.get_height()

        for i, template in enumerate(opts.values()):
            for k, v in TemplateMapper.items():
                if v == template:
                    template_name = k
                    break

            self.context_menu.options[i].on_lclick_pressed_clbck = self.add_projectile(
                template, template_name
            )

    def _build_buttons(self):
        """Constructs the buttons of the queue"""
        self.tray = []
        self.children.clear()
        ratio_x = self.rectangle.get_width() / self._origin_rect.get_width()
        ratio_y = self.rectangle.get_height() / self._origin_rect.get_height()
        button_size_x = self._buttons_orig_dim[0] * ratio_x
        button_size_y = self._buttons_orig_dim[1] * ratio_y
        space = 5 * ratio_x

        reminder = max(len(self.queue) - self.display_capacity, 0)
        self.reminder_text = TextLabel(
            Rectangle(
                self.rectangle.get_width() - button_size_x,
                0,
                self.rectangle.get_width(),
                button_size_y,
            ),
            "+" + str(reminder),
            "Orbitron-Black",
            12,
            anchor_x="left",
            anchor_y="bottom",
            halign="center",
            valign="center",
        )
        self.add_child(self.reminder_text)
        self.tray.append(self.reminder_text)

        for i in range(self.display_capacity):
            index = len(self.queue) - i - 1

            if 0 <= index < len(self.queue):
                img = pyglet.image.load(self.queue[index][0].image_path)
                img.anchor_x, img.anchor_y = 0, 0

                if self.mutable:
                    img_hov = pyglet.image.load(
                        "game/resources/gui/editor/button_x.png"
                    )
                else:
                    img_hov = img
            else:
                index = None
                img = pyglet.image.load("game/resources/gui/editor/blank.png")
                img_hov = img

            button = Button(
                Rectangle(
                    self.rectangle.get_width()
                    - (i + 1) * button_size_x
                    - i * space
                    - button_size_x,
                    0,
                    self.rectangle.get_width()
                    - i * button_size_x
                    - i * space
                    - button_size_x,
                    button_size_y,
                ),
                img,
                img_hov,
                img_hov,
            )
            button.data = index
            button.on_lclick_pressed_clbck = self.remove_projectile
            self.tray.append(button)
            self.add_child(button)
            i += 1

        if self.mutable:
            self.add_button = Button(
                Rectangle(
                    self.rectangle.get_width()
                    - (i + 1) * button_size_x
                    - i * space
                    - button_size_x,
                    0,
                    self.rectangle.get_width()
                    - i * button_size_x
                    - i * space
                    - button_size_x,
                    button_size_y,
                ),
                pyglet.image.load("game/resources/gui/editor/button_add.png"),
                pyglet.image.load("game/resources/gui/editor/button_add.png"),
                pyglet.image.load("game/resources/gui/editor/button_add.png"),
            )
            self.add_button.on_lclick_pressed_clbck = self.display_context_menu
            self.tray.append(self.add_button)
            self.add_child(self.add_button)

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)
        self._build_buttons()

        # Finally makes all of it's other children resize
        for child in self.children:
            if child not in self.tray:
                child._rescale(dx, dy)

    def add_projectile(self, projectile_template, template_name):
        """Adds projectile to the queue."""

        def callback(x, y, modifiers, obj):
            self.queue.append((projectile_template, template_name))
            self._build_buttons()

        return callback

    def display_context_menu(self, x, y, modifiers, object):
        """Displays the add context menu"""
        self.context_menu.set_visibility(True)

    def on_focus(self, focus: bool):
        """On focus change callback"""
        if not focus:
            self.context_menu.set_visibility(False)

        super().on_focus(focus)

    def remove_projectile(self, x, y, modifiers, obj):
        """Removes projectile from the queue."""
        if obj.data is not None and self.mutable:
            del self.queue[obj.data]
            self._build_buttons()

    def pop_projectile(self):
        """Pops the first projectile from the queue"""
        if self.queue:
            del self.queue[-1]
            self._build_buttons()

    def push_projectile(self, projectile_template):
        """Pushes given projectile into the queue"""
        for k, v in TemplateMapper.items():
            if isinstance(projectile_template, v):
                self.queue.append((projectile_template, k))
                self._build_buttons()
                break

    def draw(self):
        """Draws the element on the screen"""
        pass
