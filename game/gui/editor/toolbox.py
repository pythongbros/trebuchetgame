import pyglet
from pyglet import image

from game.core.geometry import Rectangle
from game.objects.templates.concrete import (
    BasicAlienTemplate,
    ArmoredAlienTemplate,
    BlockTemplate,
    GroundTemplate,
    CrateTemplate,
    BoxTemplate,
    BeamTemplate,
)
from game.objects import PhysicsObject, Projectile, DestructableObject, Mob

from ..button import Button
from ..gui_element import GUIElement


class Toolbox(GUIElement):
    """A class used for visualization of a toolbox used in the editor.

    Attrubites
    ----------
    tools : list
        The list of all available tools
    display_capacity : int
        Indicates how many tools should be visible at once
    function : callable
        A function executed when a tool was selected
    """

    def __init__(self, rect: Rectangle, display_capacity: int, function: callable):
        """Initializator builds the projectiles queue tray"""
        super().__init__(rect)
        self.tools = []
        self.function = function
        self.display_capacity = display_capacity
        self.begin_index = 0
        self._build_buttons()

    def _build_buttons(self):
        """Constructs the buttons of the queue"""
        self.children.clear()
        button_size = self.rectangle.get_height()
        space = 10

        img_aleft = pyglet.image.load("game/resources/gui/editor/button_arrowleft.png")
        img_aright = pyglet.image.load(
            "game/resources/gui/editor/button_arrowright.png"
        )

        self.tools = [
            (GroundTemplate, PhysicsObject),
            (BlockTemplate, DestructableObject),
            (CrateTemplate, DestructableObject),
            (BasicAlienTemplate, Mob),
            (ArmoredAlienTemplate, Mob),
            (BoxTemplate, DestructableObject),
            (BeamTemplate, DestructableObject),
        ]
        self.buttons = []

        button_left = Button(
            Rectangle(0, 0, button_size, button_size), img_aleft, img_aleft, img_aleft
        )
        button_left.on_lclick_pressed_clbck = self.decrement_begin_index
        self.add_child(button_left)
        self.buttons.append(button_left)

        i = button_size + space
        for index in range(self.display_capacity):
            if self.begin_index + index >= len(self.tools):
                break

            img = pyglet.image.load(self.tools[self.begin_index + index][0].image_path)
            button = Button(
                Rectangle(i, 0, i + button_size, button_size), img, img, img
            )
            button.data = self.tools[self.begin_index + index]
            button.on_lclick_pressed_clbck = self.selectTool
            self.add_child(button)
            self.buttons.append(button)
            i += button_size + space

        button_right = Button(
            Rectangle(i, 0, i + button_size, button_size),
            img_aright,
            img_aright,
            img_aright,
        )
        button_right.on_lclick_pressed_clbck = self.increment_begin_index
        self.add_child(button_right)
        self.buttons.append(button_right)

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)
        self._build_buttons()

        for child in self.children:
            if child not in self.buttons:
                child._rescale(dx, dy)

    def increment_begin_index(self, x, y, modifiers, obj):
        """Increments the begin index"""
        self.begin_index = min(
            self.begin_index + 1, len(self.tools) - self.display_capacity
        )
        self._build_buttons()

    def decrement_begin_index(self, x, y, modifiers, obj):
        """Increments the begin index"""
        self.begin_index = max(self.begin_index - 1, 0)
        self._build_buttons()

    def selectTool(self, x, y, modifiers, obj):
        self.function(obj.data)

    def draw(self):
        """Draws the element on the screen"""
        pass
