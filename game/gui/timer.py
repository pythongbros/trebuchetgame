from pyglet import clock

from game.core.geometry import Rectangle
from .text_label import TextLabel


class Timer(TextLabel):
    """This element is used to count time.

    This component resembles a simple clock.
    If you want to count decimals, you should
    consider using the Counter class.

    Attributes
    ----------
    seconds: int
        Number of counted seconds
    minutes: int
        Number of counted minutes
    hours: int
        Number of counted hours

    running: bool
        True, if time is actually being measured

    last_tick_time : float
        A fraction of second that passed from last Timer tick
    """

    def __init__(
        self,
        rect: Rectangle,
        seconds: int = 0,
        minutes: int = 0,
        hours: int = 0,
    ):
        """Initializator creates Timer using given parameters"""
        self.seconds = seconds
        self.minutes = minutes
        self.hours = hours

        self.running = False
        self.last_tick_time = 0.0

        super().__init__(rect, "")
        self.fetch_time()

        # Schedule the timer to update every tick:
        clock.schedule(self._tick)

    def set_text(self, text: str):
        raise NotImplementedError("Cannot set text of a Timer: use set_time() instead!")

    def set_time(self, secs: int, mins: int, hrs: int):
        """Sets timer to a given timestemp"""
        self.seconds = secs % 60
        self.minutes = (mins + secs // 60) % 60
        self.hours = hrs + (mins + secs // 60) // 60

        self.fetch_time()

    def get_secs(self):
        return self.seconds + 60 * self.minutes + 360 * self.hours

    def fetch_time(self):
        """Refreshes the clock after hours/minutes/seconds change"""
        hours_str = str(self.hours).zfill(2)
        minutes_str = str(self.minutes).zfill(2)
        seconds_str = str(self.seconds).zfill(2)
        super().set_text(hours_str + ":" + minutes_str + ":" + seconds_str)

    def add(self, secs: int, mins: int, hrs: int):
        """Add given amount of time to the timer"""
        self.set_time(self.seconds + secs, self.minutes + mins, self.hours + hrs)

    def subtract(self, secs: int, mins: int, hrs: int):
        """Subtract given amount of time from the timer"""
        self.set_time(self.seconds - secs, self.minutes - mins, self.hours - hrs)

    def start(self):
        """Start measuring time"""
        self.running = True

    def pause(self):
        """Pause measuring time"""
        self.running = False

    def toggle(self):
        """Toggles the timer on/off"""
        self.running = not self.running

    def reset(self):
        """Resets the timer and stops counting"""
        self.running = False
        self.last_tick_time = 0.0
        self.set_time(0, 0, 0)

    def _tick(self, dt):
        """Performs the Timer tick whenever a second passed"""
        if self.running:
            self.last_tick_time += dt

            if self.last_tick_time >= 1.0:
                self.set_time(self.seconds + 1, self.minutes, self.hours)
                self.last_tick_time = 0
