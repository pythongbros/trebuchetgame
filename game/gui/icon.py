from pyglet.image import ImageData

from game.core.geometry import Rectangle
from game.utils.graphics import scale_image
from .gui_element import GUIElement


class Icon(GUIElement):
    """This class is a representation of an icon.

    What we will call an icon here is simply an
    image that can be displayed as a gui element.

    Attributes
    ----------
    _src_img : ImageData
        The original image stored to prevent quality loss while scaling the element
    image : ImageData
        Actual image to be displayed
    """

    def __init__(self, rect: Rectangle, img: ImageData):
        """Initializator that takes the rectangle to draw the image in"""
        super().__init__(rect)
        self._src_img = img
        self.image = scale_image(self._src_img, rect.get_width(), rect.get_height())

    def set_image(self, img: ImageData):
        """Sets a new image"""
        self._src_img = img
        self.image = scale_image(
            self._src_img, self.rectangle.get_width(), self.rectangle.get_height()
        )

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)
        self.image = scale_image(
            self._src_img, self.rectangle.get_width(), self.rectangle.get_height()
        )

        # Finally makes all of it's children resize
        for child in self.children:
            child._rescale(dx, dy)

    def draw(self):
        """Draws the element on the screen"""
        self.image.blit(
            self.get_absolute_rect().lower_left.x,
            self.get_absolute_rect().lower_left.y,
            0,
        )
