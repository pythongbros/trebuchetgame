from pyglet.image import ImageData

from game.core.geometry import Rectangle
from game.utils.graphics import scale_image
from .gui_element import GUIElement


class ProgressBar(GUIElement):
    """This is a class that resembles a progress bar.

    It can be used as an actual progress bar or
    for example as a health bar.

    Attributes
    ----------
    img_frame: ImageData
        Image of the frame of this bar
    img_fill: ImageData
        Image of the filling of this bar

    max_value: float
        The maximal value considered as 100% of the bar
    current_value: float
        The current value of the bar filling

    offset_x: int
        Pixel offset of the filling from the frame in the x axis
    offset_y: int
        Pixel offset of the filling from the frame in the y axis
    _percentage: float
        Precentage (0-1) of the filled part of the bar
    """

    def __init__(
        self,
        rect: Rectangle,
        img_frame: ImageData,
        img_fill: ImageData,
        max_value: float,
        init_value: float,
        offset_x: int,
        offset_y: int,
    ):
        """Initializator creates the bar from given parameters.

        Parameters
        ----------
        rect: Rectangle
            The rectangle to  draw the bar in
        _src_img_frame: ImageData
            Original image of the bar frame
        img_frame: ImageData
            Image of the bar frame being currently displayed (scaled)
        _src_img_fill: ImageData
            Original image of the bar filling
        img_fill_scaled: ImageData
            Scaled image of the bar filling
        max_value: float
            The maximal value considered as 100% of the bar
        init_value: float
            The initial value of the bar filling

        _origin_offset_x : int
            Original pixel offset of the filling from the frame along the x axis
        _origin_offset_y : int
            Original pixel offset of the filling from the frame along the y axis
        offset_x: int
            Current pixel offset of the filling from the frame along the x axis
        offset_y: int
            Current pixel offset of the filling from the frame along the y axis
        """
        super().__init__(rect)

        self.max_value = max_value
        self.current_value = init_value

        self._origin_offset_x = offset_x * (rect.get_width() / img_frame.width)
        self._origin_offset_y = offset_y * (rect.get_height() / img_frame.height)
        self.offset_x = self._origin_offset_x
        self.offset_y = self._origin_offset_y

        self._src_img_frame = img_frame
        self.img_frame = scale_image(
            self._src_img_frame, rect.get_width(), rect.get_height()
        )
        self._src_img_fill = img_fill
        self.img_fill_scaled = img_fill

        self._percentage = 0
        self.fetch_filling()

    def fetch_filling(self):
        """Fetches the bar filling to represent the current percentage"""
        self._percentage = self.current_value / self.max_value

        self.img_fill_scaled = scale_image(
            self._src_img_fill,
            int((self.rectangle.get_width() - 2 * self.offset_x) * self._percentage),
            self.rectangle.get_height() - 2 * self.offset_y,
        )

    def add_progress(self, progress_val: float):
        """Adds specified amount of progress to the bar"""
        new_value = self.current_value + progress_val
        self.current_value = min(self.max_value, new_value)
        self.current_value = max(0, self.current_value)

        self.fetch_filling()

    def subtract_progress(self, progress_val: float):
        """Subtracts specified amount of progress from the bar"""
        new_value = self.current_value - progress_val
        self.current_value = min(self.max_value, new_value)
        self.current_value = max(0, self.current_value)

        self.fetch_filling()

    def set_progress(self, progress_val: float):
        """Sets the progress value"""
        new_value = progress_val
        self.current_value = min(self.max_value, new_value)
        self.current_value = max(0, self.current_value)

        self.fetch_filling()

    def _rescale(self, dx: float, dy: float):
        """Rescales the element when the viewport was resized; (dx, dy) is the scale ratio"""
        self.rectangle = self._origin_rect.get_scaled_relatively(dx, dy)
        self.offset_x = self._origin_offset_x * dx
        self.offset_y = self._origin_offset_y * dy

        self.img_frame = scale_image(
            self._src_img_frame, self.rectangle.get_width(), self.rectangle.get_height()
        )

        self.fetch_filling()

        # Finally makes all of it's children resize
        for child in self.children:
            child._rescale(dx, dy)

    def draw(self):
        self.img_frame.blit(
            self.get_absolute_rect().lower_left.x,
            self.get_absolute_rect().lower_left.y,
            0,
        )

        if self._percentage > 0:
            self.img_fill_scaled.blit(
                self.get_absolute_rect().lower_left.x + self.offset_x,
                self.get_absolute_rect().lower_left.y + self.offset_y,
                0,
            )
