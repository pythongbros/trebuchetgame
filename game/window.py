import pyglet
from pyglet.gl import gl
from game.connectors import ServerConnector


class Window(pyglet.window.Window):
    """Main window class for app"""

    def __init__(self, initial_scene, width, height, name, **kwargs):
        self.original_dim = (width, height)
        self.current_scene = initial_scene(self)
        self.current_scene.setup()

        super().__init__(width, height, name, **kwargs)

    def on_draw(self):
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        self.current_scene.on_draw()

        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

    def update(self, dt):
        self.current_scene.update(dt)

    def on_resize(self, width, height):
        """This is a handler which is called every time when the window is being resized"""
        gl.glViewport(0, 0, width, height)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(0, width, 0, height, -1, 1)
        gl.glMatrixMode(gl.GL_MODELVIEW)

        self.current_scene.gui_manager.notify_resize(width, height)

    def change_scene(self, scene):
        self.current_scene = scene
        self.current_scene.setup()
        self.on_resize(self.width, self.height)
