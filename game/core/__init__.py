from .geometry import Point, Rectangle

__all__ = ["Point", "Rectangle"]
