import math


class Point:
    """Representation of a point in 2D space.

    Attributes
    ----------
    x : float
        x coordinate
    float : float
        y coordinate
    """

    def __init__(self, x, y):
        """Constructs a point from x and y coords"""
        self.x = x
        self.y = y

    def get_distance_to(self, point):
        """Returns distance to another point"""
        return math.sqrt((self.x - point.x) ** 2 + (self.y - point.y) ** 2)

    def get_as_tuple(self):
        """Retruns point as a tuple"""
        return self.x, self.y


class Rectangle:
    """This class represents a rectangle in 2D space

    Attributes
    ----------
    x : float
        x coordinate
    y : float
        y coordinate
    """

    def __init__(self, x1: float, y1: float, x2: float, y2: float):
        """Constructs a rectangle from two given points:
        lower left corner (x1, y1) and upper right corner (x2, y2)"""

        # Check if points are in correct order:
        if x1 > x2 or y1 > y2:
            raise ValueError(
                "Given points should be such that: x1 <= x2 and y1 <= y2 !"
            )

        # Lower left corner point:
        self.lower_left = Point(x1, y1)

        # Upper right corner point:
        self.upper_right = Point(x2, y2)

    def get_height(self):
        """Returns height of the rectangle"""
        return abs(self.upper_right.y - self.lower_left.y)

    def get_width(self):
        """Returns width of the rectangle"""
        return abs(self.upper_right.x - self.lower_left.x)

    def get_diagonal(self):
        """Returns the lenght of diagonal in this rectangle"""
        return self.lower_left.get_distance_to(self.upper_right)

    def get_center(self):
        """Returns the center point of the rectangle"""
        return Point(
            self.lower_left.x + self.get_width() / 2,
            self.lower_left.y + self.get_height() / 2,
        )

    def get_scaled(self, scale_x, scale_y):
        """Returns the rectangle scaled to fit the (scale_x, scale_y) ratio

        We do not support negative scale for a simple reason:
        that scaling operation would possibly switch the lower
        left corner and the upper right corner.
        We also don't want to degenerate the rectangle by scaling it
        by zero.
        """

        if scale_x <= 0 or scale_y <= 0:
            raise ValueError("Negative and zero scaling are not supported!")

        return Rectangle(
            self.lower_left.x,
            self.lower_left.y,
            self.lower_left.x + int(self.get_width() * scale_x),
            self.lower_left.y + int(self.get_height() * scale_y),
        )

    def get_scaled_relatively(self, scale_x, scale_y):
        """Returns the rectangle scaled to fit the (scale_x, scale_y) ratio, including changing it's position

        We do not support negative scale for a simple reason:
        that scaling operation would possibly switch the lower
        left corner and the upper right corner.
        We also don't want to degenerate the rectangle by scaling it
        by zero.
        """

        if scale_x <= 0 or scale_y <= 0:
            raise ValueError("Negative and zero scaling are not supported!")

        return Rectangle(
            int(self.lower_left.x * scale_x),
            int(self.lower_left.y * scale_y),
            int(self.upper_right.x * scale_x),
            int(self.upper_right.y * scale_y),
        )

    def is_point_inside(self, point: Point):
        """Returns True if given point is inside the rectangle"""
        return (self.lower_left.x <= point.x <= self.upper_right.x) and (
            self.lower_left.y <= point.y <= self.upper_right.y
        )

    def get_as_tuple_pts(self):
        """Returns the tuple of upper left corner point and lower right corner point"""
        return self.lower_left, self.upper_right

    def get_as_tuple_coords(self):
        """Returns the tuple of upper left corner point coords and lower right corner point coords"""
        return *self.lower_left.get_as_tuple(), *self.upper_right.get_as_tuple()
