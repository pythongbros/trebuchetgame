from typing import Tuple

import cv2

import pymunk
from pymunk import Vec2d
from pymunk import autogeometry


def get_center_point(poly_points: list, axis: int):
    """Returns axis center point from array"""
    x_max = max(poly_points, key=lambda x: x[axis])[axis]
    x_min = min(poly_points, key=lambda x: x[axis])[axis]
    return (x_max + x_min) / 2


def collision_shape_from_image(
    image_path: str, dimension: Tuple[int], body: pymunk.Body, threshold: int = 240
):
    """Method preparing polygon pymunk shape based on given image - binary mask 0-alpha channel"""

    # Read image and add alpha channel if necessary
    image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
    image = cv2.resize(image, dimension, interpolation=cv2.INTER_AREA)

    # Generate contour vertices
    mask = cv2.threshold(image[:, :, 3], 0, threshold, cv2.THRESH_BINARY)[1]
    contours = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]

    # Generate hull
    poly_points = [(x[0, 0], x[0, 1]) for x in contours[0]]
    hull = autogeometry.to_convex_hull(poly_points, 0.1)

    # Calculate center and offset
    center = Vec2d(get_center_point(poly_points, 0), get_center_point(poly_points, 1))
    offset = (dimension[0] / 2 - center.x, dimension[1] / 2 - center.y)

    return (
        pymunk.shapes.Poly(body, hull, transform=pymunk.Transform(d=-1)),
        offset,
    )
