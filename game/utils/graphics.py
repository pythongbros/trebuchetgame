from pyglet.gl import gl
from pyglet.image import ImageData


def scale_image(image: ImageData, width: int, height: int):
    """Function to scale an image to given dimensions"""

    texture = image.get_texture()
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)

    texture.width = width
    texture.height = height
    image.width = width
    image.height = height

    return image
