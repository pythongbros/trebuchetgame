def pts_to_pixel(pts: int):
    """Converts points to pixels"""
    return pts * (4 / 3)
