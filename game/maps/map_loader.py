import os
import io
import json
import base64

import pyglet
import pymunk

from .map import Map, MapState
from game.system import ObjectManager
from game.objects import PhysicsObject, Projectile, DestructableObject, Mob, Player
from game.objects.templates.concrete import (
    SpaceshipTemplate,
    GroundTemplate,
    BlockTemplate,
    BasicAlienTemplate,
    ProjectileTemplate,
    LargeProjectileTemplate,
    AcceleratingProjectileTemplate,
    ZeroGravityProjectileTemplate,
    TeleportingProjectileTemplate,
)


class MapLoader:
    """Map loader class (singleton)

    This class is supposed to hold all available maps and load them from
    binaries. They should at least consist of object manager and pymunk space.
    This fetching procedure should be available by id.
    """

    map_dir = "game/maps/map_files"
    maps = []

    @staticmethod
    def update_map_data(id: int, data: dict):
        pass

    @staticmethod
    def load_maps_data(refresh: bool):
        from game.connectors import ServerConnector

        if refresh:
            MapLoader.maps = []

        try:
            if not MapLoader.maps:

                # Send request to server to get maps
                code, server_maps = ServerConnector.get_maps_data()

                if code != 200:
                    return []

                # Walk over local dir to retrieve maps
                for directory in (
                    f.path for f in os.scandir(MapLoader.map_dir) if f.is_dir()
                ):

                    with open(directory + "/info.json") as file:
                        data = json.load(file)

                    if os.path.exists(image_path := directory + "/image.png"):
                        data["image"] = pyglet.image.load(image_path)
                    else:
                        data["image"] = pyglet.image.load(
                            "game/resources/gui/planets/planet6.png"
                        )

                    MapLoader.maps.append(Map(directory, **data))

                # Set maps states
                for server_map in server_maps:
                    for _map in MapLoader.maps:
                        if server_map["_id"] == _map._id:
                            _map.state = MapState.BOTH
                            break
                    else:
                        server_map["state"] = MapState.SERVER

                        if server_map["image"] is None:
                            server_map["image"] = pyglet.image.load(
                                "game/resources/gui/planets/planet6.png"
                            )
                        else:
                            fileobj = base64.b64decode(server_map["image"])
                            server_map["image"] = pyglet.image.load(
                                "sample.png", file=io.BytesIO(fileobj)
                            )

                        MapLoader.maps.append(Map(None, **server_map))

        except Exception:
            return []

        return MapLoader.maps

    @staticmethod
    def load_map(width: int, height: int, map: Map) -> ObjectManager:

        if map.state == MapState.SERVER:
            return None, "Download map first"

        space = pymunk.Space()
        obj_manager = ObjectManager(space, width, height)

        success = map.register(obj_manager)

        if not success:
            return None, "Cannot load map"

        return obj_manager, "Map loaded"
