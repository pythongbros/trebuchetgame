from copy import copy
from enum import Enum
import base64
import hashlib
import json

import pyglet

from game.system import ObjectManager
from game.objects import PhysicsObject, DestructableObject, Projectile, Mob, Player
from game.objects.templates.concrete.mapper import TemplateMapper


class MapState(Enum):
    SERVER = 1
    LOCAL = 2
    BOTH = 3

    def get_image(self):
        root = "game/resources/gui/"
        if self.value == 1:
            return pyglet.image.load(root + "download_icon.png")
        elif self.value == 2:
            return pyglet.image.load(root + "send_icon.png")
        else:
            return pyglet.image.load(root + "ok_icon.png")


class Map:
    def __init__(
        self,
        directory: str,
        _id: int,
        name: str,
        description: str,
        image: pyglet.image.ImageData,
        state: MapState = MapState.LOCAL,
    ):
        self.directory = directory
        self._id = _id
        self.name = name
        self.description = description
        self.image = image
        self.state = state

        # HARDCODED !!!
        self.planet_image_path = "game/resources/gui/editor/img_planet_default.png"
        self.background_image_path = "game/resources/gui/editor/img_back_default.png"

        self.class_mapper = {
            "physics_objects": PhysicsObject,
            "destructable_objects": DestructableObject,
            "projectiles": Projectile,
            "mobs": Mob,
        }

    def register(self, obj_manager: ObjectManager):

        try:
            # Load object json from file
            with open(self.directory + "/object.json") as file:
                map_object = json.load(file)

            # Parse into object_manager object
            obj_manager.space.gravity = tuple(map_object["gravity"])
            projectile_pos = self._load_player(map_object["player"], obj_manager)

            self.projectile_tray = []

            for k, v in self.class_mapper.items():
                self._load_class(map_object[k], v, obj_manager, projectile_pos)

        except Exception:
            return False

        return True

    def _load_player(self, player_dict: dict, obj_manager: ObjectManager):
        player_dict = copy(player_dict)
        player_dict["position"] = tuple(player_dict["position"])

        template = TemplateMapper[player_dict["template"]]
        del player_dict["template"]

        player = Player.from_template(template(**player_dict))
        obj_manager.register(player)

        return player.get_absolute_projectile_position()

    def _load_class(
        self,
        objects: list,
        obj_class: callable,
        obj_manager: ObjectManager,
        projectile_pos: tuple,
    ):

        for obj_dict in objects:
            obj_dict_copy = copy(obj_dict)

            template = TemplateMapper[obj_dict_copy["template"]]
            del obj_dict_copy["template"]

            if obj_class == Projectile:
                obj_dict_copy["position"] = projectile_pos

                obj_template = template(**obj_dict_copy)
                self.projectile_tray.append(obj_template)

                obj_manager.register_projectile(obj_class.from_template(obj_template))
            else:
                obj_dict_copy["position"] = tuple(obj_dict_copy["position"])
                obj_manager.register(obj_class.from_template(template(**obj_dict_copy)))

    def hash(self):
        with open(self.directory + "/object.json", "rb") as f:
            encoded_file = base64.b64encode(f.read())
            file_hash = hashlib.sha256(encoded_file).hexdigest()

        return file_hash
