import logging

import pymunk
import pyglet
from pyglet.gl import glTranslatef, glPushMatrix, glPopMatrix

from game.objects.collision import CollisionHandler
from game.objects import SceneObject, PhysicsObject, CollidingObject, Mob
from .abstract_manager import AbstractManager


logger = logging.getLogger(__name__)


class ObjectManager(AbstractManager):
    """Manager class defined to handle all physics objects

    You should not interfere with the code inside this class,
    it's just a high-level api to provide you necessary functions
    outside.

    Attributes
    ----------
    elements : list
        List of all registered elements

    _origin_screen_size : Tuple[int]
        This is the size of so-called 'origin screen'

        Notice that this program may be run on various machines
        with different screen resolutions. The problem is to keep
        the original layout of the scene (which has been designed by
        the producer), regardless of the desktop resolution the
        user has.

        You may think of the origin screen as a original canvas,
        on which designers put their work during the development process.
        It is some kind of reference, which tells the program what was
        the original size of the scene, so it can rescale it properly
        to fit your desktop resolution.

    collision_handler: CollisionHandler
        Collision Handler instance of this scene

    playing : bool
        True if playing mode is on - activates mob kill counter
    """

    def __init__(
        self,
        space: pymunk.space,
        screen_width: int,
        screen_height: int,
        origin_width: int = 1920,
        origin_height: int = 1080,
    ):
        """Initializator for Object Manager"""
        super().__init__(screen_width, screen_height, origin_width, origin_height)
        self.space = space
        self.projectiles = []
        self.current_projectile = None
        self.window = None
        self.collision_handler = CollisionHandler()
        self.collision_handler.mount_handlers([self.space])
        self.playing = False
        self.projectile_lock = False

    def next_projectile(self):
        """Add next projectile to space and elements"""

        def projectile_change(dt):
            # Remove current projectile handlers
            if self.current_projectile is not None:
                self.window.remove_handlers(self.current_projectile.on_mouse_press)
                self.window.remove_handlers(self.current_projectile.on_mouse_drag)
                self.window.remove_handlers(self.current_projectile.on_mouse_release)

            # If there are projectiles left, switch to next and setup handlers
            if self.projectiles:
                self.current_projectile = self.projectiles.pop()
                self.register(self.current_projectile)

                self.window.push_handlers(self.current_projectile.on_mouse_press)
                self.window.push_handlers(self.current_projectile.on_mouse_drag)
                self.window.push_handlers(self.current_projectile.on_mouse_release)

            # If no projectiles left - notify window to end map
            else:
                self.current_projectile = None
                self.window.current_scene.start_countdown()

            self.projectile_lock = False

        if not self.projectile_lock:
            self.projectile_lock = True
            pyglet.clock.schedule_once(projectile_change, 1.0)

    def register(self, element: SceneObject, group_name: str = None):
        """Adds element to the element list and space"""
        self.elements.append(element)

        if group_name is not None:
            self.add_to_group(element, group_name)

        if isinstance(element, PhysicsObject):
            if element.shape.body.body_type == pymunk.Body.STATIC:
                self.space.add(element.shape)
            else:
                self.space.add(element.shape, element.shape.body)

        if isinstance(element, CollidingObject):
            self.collision_handler.mount_object(element)

        logger.info(f"Element added to the scene: {element}")

    def register_projectile(self, projectile):
        """Adds projectile to projectile list"""
        self.projectiles.append(projectile)

    def unregister(self, element):
        """Removes an element from the element list and space"""
        if element not in self.elements:
            logger.warning("Element not registered - cannot unregister")
            return None

        for group in self.groups.keys():
            if element in self.groups[group]:
                self.groups[group].remove(element)

        if isinstance(element, PhysicsObject):
            if element.shape.body.body_type == pymunk.Body.STATIC:
                self.space.remove(element.shape)
            else:
                self.space.remove(element.shape.body, element.shape)

        if isinstance(element, CollidingObject):
            self.collision_handler.dismount_object(element)

        if self.current_projectile == element:
            self.next_projectile()

        self.elements.remove(element)
        logger.info(f"Element removed from the scene: {element}")

        if isinstance(element, Mob) and self.playing:
            self.window.current_scene.mobs_killed += 1

            if not any(isinstance(e, Mob) for e in self.elements):
                self.window.current_scene.start_countdown()

    def unregister_all(self):
        """Removes all elements from the element list and space"""
        while self.elements:
            self.unregister(self.elements[0])

    def unregister_all_from(self, group_name: str):
        """Removes all elements from given group from the element list and space"""
        while self.groups[group_name]:
            self.unregister(self.groups[group_name][0])

    def get_object_at(self, x, y):
        """Returns first encountered object at point (x, y)"""
        for element in reversed(self.elements):
            if element.is_point_inside(x, y):
                return element

        return None

    def is_on_canvas(self, element: SceneObject):
        """Checks if given element is inside the canvas"""
        offset = max(element.image.width, element.image.height)

        return (
            -offset < element.position[0] < self._origin_screen_size[0] + offset
        ) and (-offset < element.position[1])

    def get_mobs_num(self):
        """Get number of mobs on the board"""
        mobs = 0
        for elem in self.elements:
            if isinstance(elem, Mob):
                mobs += 1

        return mobs

    def clear_space(self):
        """Removes redundant objects from the scene"""
        for elem in self.elements:
            if elem.destroyed or (not self.is_on_canvas(elem) and not elem.permament):
                self.unregister(elem)

    def update_all(self):
        """This method updates all the registered elements"""
        self.clear_space()

        if (
            self.current_projectile is not None
            and self.current_projectile.shot
            and self.current_projectile.action is None
        ):
            self.next_projectile()

        for elem in self.elements:
            elem.update_all()

    def draw_all(self, translation: tuple = (0, 0, 0)):
        """This method is responsible for drawing all registered elements"""
        glPushMatrix()
        glTranslatef(*translation)

        for elem in self.elements:
            elem.draw_all()

        glPopMatrix()

    def draw_group(self, group_name, translation: tuple = (0, 0, 0)):
        """Draw all registered elements from the specified group"""
        glPushMatrix()
        glTranslatef(*translation)

        for elem in self.groups[group_name]:
            elem.draw_all()

        glPopMatrix()

    def update_group(self, group_name):
        """Update all registered elements from the specified group"""
        self.clear_space()

        if (
            self.current_projectile is not None
            and self.current_projectile.shot
            and self.current_projectile.action is None
        ):
            self.next_projectile()

        for elem in self.groups[group_name]:
            elem.update_all()
