from typing import Tuple

from game.core import Point, Rectangle
from game.gui import GUIElement
from .abstract_manager import AbstractManager

from pyglet.gl import glTranslatef, glPushMatrix, glPopMatrix


class GUIManager(AbstractManager):
    """Manager class defined to handle all gui events

    You should not interfere with the code inside this class,
    it's just a high-level api to provide you necessary functions
    outside. To change the behaviour od your objects you should
    rather use GUIElement's callbacks.

    Attributes
    ----------
    elements : list
        List of all registered elements
    focus : GUIElement
        Currently focused GUI element

    _origin_screen_size : Tuple[int]
        This is the size of so-called 'origin screen'

        Notice that this program may be run on various machines
        with different screen resolutions. The problem is to keep
        the original layout of the scene (which has been designed by
        the producer), regardless of the desktop resolution the
        user has.

        You may think of the origin screen as a original canvas,
        on which designers put their work during the development process.
        It is some kind of reference, which tells the program what was
        the original size of the scene, so it can rescale it properly
        to fit your desktop resolution.

    mouse_pos : Tuple[int]
        Last recorded mouse position

    on_mouse_press_clbck : callable
        On mouse press additional callback
    on_mouse_release_clbck : callable
        On mouse release additional callback
    on_mouse_motion_clbck : callable
        On mouse motion additional callback
    on_key_press_clbck : callable
        On key press additional callback
    on_key_release_clbck : callable
        On key release additional callback
    """

    def __init__(
        self,
        screen_width: int,
        screen_height: int,
        origin_width: int = 1920,
        origin_height: int = 1080,
    ):
        """Initializator for GUI Manager"""
        super().__init__(screen_width, screen_height, origin_width, origin_height)
        self.focus = None
        self.mouse_pos = None

        self.on_mouse_press_clbck = lambda *_: None
        self.on_mouse_release_clbck = lambda *_: None
        self.on_mouse_motion_clbck = lambda *_: None
        self.on_key_press_clbck = lambda *_: None
        self.on_key_release_clbck = lambda *_: None
        self.on_mouse_scroll_clbck = lambda *_: None

    def register(self, element, group_name: str = None):
        """Adds element to the element list"""
        if element not in self.elements:
            self.elements.append(element)

        if group_name is not None:
            self.add_to_group(element, group_name)

    def unregister(self, element):
        """Removes an element from the element list"""
        if element in self.elements:
            self.elements.remove(element)

        for group in self.groups.keys():
            if element in self.groups[group]:
                self.groups[group].remove(element)

    def unregister_all(self):
        """Removes all elements from the element list"""
        while self.elements:
            self.unregister(self.elements[0])

    def unregister_all_from(self, group_name: str):
        """Removes all elements from given group from the element list"""
        while self.groups[group_name]:
            self.unregister(self.groups[group_name][0])

    def move_element_up(self, element):
        """Moves an element to an upper layer"""
        if element in self.elements:
            index = self.elements.index(element)
            if index != len(self.elements) + 1:
                self.elements.insert(index + 1, self.elements.pop(index))

    def move_element_down(self, element):
        """Moves an element to a lower layer"""
        if element in self.elements:
            index = self.elements.index(element)
            if index != 0:
                self.elements.insert(index - 1, self.elements.pop(index))

    def update_all(self):
        """This method updates all the registered elements"""
        for elem in self.elements:
            elem.update_all()

    def draw_all(self, translation: tuple = (0, 0, 0)):
        """This method is responsible for drawing all registered elements"""
        glPushMatrix()
        glTranslatef(*translation)

        for elem in self.elements:
            if elem.is_visible:
                elem.draw_all()

        glPopMatrix()

    def draw_group(self, group_name, translation: tuple = (0, 0, 0)):
        """This method is responsible for drawing all registered elements in a specified group"""
        glPushMatrix()
        glTranslatef(*translation)

        for elem in self.groups[group_name]:
            if elem.is_visible:
                elem.draw_all()

        glPopMatrix()

    def update_group(self, group_name):
        """Update all registered elements from the specified group"""
        for elem in self.groups[group_name]:
            elem.update_all()

    def notify_resize(self, width, height):
        """Informs all of the elements that window size has changed"""

        # Compute the scale ratio:
        dx = width / self._origin_screen_size[0]
        dy = height / self._origin_screen_size[1]

        for elem in self.elements:
            elem._rescale(dx, dy)

    def set_focus(self, elem: GUIElement, x, y):
        """Sets focus to given element or None"""
        if self.focus != elem:
            if self.focus is not None:
                self.focus.on_focus(False)

            self.focus = elem

            if self.focus is not None:
                elem.on_focus(True)

    def on_mouse_press(self, x, y, button, modifiers):
        """Tells elements that mouse button was pressed"""
        clicked = None

        for elem in self.elements:
            if elem.is_visible and self._is_point_inside_component(elem, x, y):
                if button == 1:
                    clicked = elem
                    elem.on_lclick_pressed(x, y, modifiers)
                elif button == 4:
                    elem.on_rclick_pressed(x, y, modifiers)

        self.set_focus(clicked, x, y)
        self.on_mouse_press_clbck(x, y, button, modifiers)

    def on_mouse_release(self, x, y, button, modifiers):
        """Informs elements that mouse button was released"""
        for elem in self.elements:
            if elem.is_visible and self._is_point_inside_component(elem, x, y):
                if button == 1:
                    elem.on_lclick_released(x, y, modifiers)
                elif button == 4:
                    elem.on_rclick_released(x, y, modifiers)

        self.on_mouse_release_clbck(x, y, button, modifiers)

    def on_mouse_motion(self, x, y, dx, dy):
        """Informs elements that mouse was moved"""
        self.mouse_pos = x, y

        for elem in self.elements:
            if elem.is_visible:
                elem.handle_mouse_motion(x, y, dx, dy)

        self.on_mouse_motion_clbck(x, y, dx, dy)

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        """Informs elements that mouse wheel was scrolled"""
        for elem in self.elements:
            if elem.is_visible:
                elem.on_mouse_scroll(x, y, scroll_x, scroll_y)

        self.on_mouse_scroll_clbck(x, y, scroll_x, scroll_y)

    def _get_component(self, element):
        """Gets the whole component the element belongs to, that is:
        the element itself, it's children, it's children's children etc."""
        component = [element]
        iterator = 0

        while iterator != len(component):
            component += component[iterator].children
            iterator += 1

        return component

    def _is_point_inside_component(self, element, x, y, ignore_visibility=False):
        """Returns True if point is inside the element or it's child"""
        for elem in self._get_component(element):
            if elem.get_absolute_rect().is_point_inside(Point(x, y)) and (
                ignore_visibility or elem.is_visible
            ):
                return True
        return False

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        """Informs elements that mouse drag happened"""
        self.on_mouse_motion(x, y, dx, dy)

    def on_key_press(self, symbol, modifiers):
        """On key press handler"""
        # The key press affects only focused gui element:
        if self.focus is not None:
            self.focus.on_key_press(symbol, modifiers)

        self.on_key_press_clbck(symbol, modifiers)

    def on_key_release(self, symbol, modifiers):
        """On key release handler"""
        # The key release affects only focused gui element:
        if self.focus is not None:
            self.focus.on_key_release(symbol, modifiers)

        self.on_key_release_clbck(symbol, modifiers)

    def on_text(self, text):
        """On text input handler"""
        # The text input affects only focused gui element:
        if self.focus is not None:
            self.focus.on_text(text)
