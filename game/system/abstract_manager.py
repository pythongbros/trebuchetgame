from abc import abstractmethod


class AbstractManager:
    """Abstract manager class defining basic manager structure

    Prefered way to deal with Managers it to have one
    for a scene; then it stores all the objects/elements
    present on the scene (using register method) and
    controls their behaviour.

    Attributes
    ----------
    elements : list
        List of all registered elements
    focus : GUIElement
        Currently focused GUI element

    groups : dict
        Dictionary containing all element groups

    _origin_screen_size : Tuple[int]
        This is the size of so-called 'origin screen'

        Notice that this program may be run on various machines
        with different screen resolutions. The problem is to keep
        the original layout of the scene (which has been designed by
        the producer), regardless of the desktop resolution the
        user has.

        You may think of the origin screen as a original canvas,
        on which designers put their work during the development process.
        It is some kind of reference, which tells the program what was
        the original size of the scene, so it can rescale it properly
        to fit your desktop resolution.
    """

    def __init__(
        self,
        screen_width: int,
        screen_height: int,
        origin_width: int = 1920,
        origin_height: int = 1080,
    ):

        self.elements = []
        self.groups = {}
        self._origin_screen_size = (origin_width, origin_height)

    @abstractmethod
    def register(self, element, group_name=None):
        """Register element in elements list"""
        raise NotImplementedError()

    @abstractmethod
    def unregister(self, element):
        """Unregister element in elements list"""
        raise NotImplementedError()

    @abstractmethod
    def update_all(self):
        """Update all registered elements"""
        raise NotImplementedError()

    @abstractmethod
    def draw_all(self, translation: tuple):
        """Draw all registered elements"""
        raise NotImplementedError()

    @abstractmethod
    def draw_group(self, group_name, translation: tuple):
        """Draw all registered elements from the specified group"""
        raise NotImplementedError()

    @abstractmethod
    def update_group(self, group_name):
        """Update all registered elements from the specified group"""
        raise NotImplementedError()

    def create_group(self, group_name):
        """Create a new group"""
        if group_name not in self.groups.keys():
            self.groups[group_name] = []

    def delete_group(self, group_name):
        """Delete an existing group"""
        if group_name in self.groups.keys():
            del self.groups[group_name]

    def add_to_group(self, element, group_name):
        """Add registered element to a group"""
        if element not in self.elements:
            raise Exception(
                "Cannot add to group "
                + group_name
                + ". Element must be registered first!"
            )

        if group_name in self.groups.keys() and element not in self.groups[group_name]:
            self.groups[group_name].append(element)

    def remove_from_group(self, element, group_name):
        """Remove element from a group"""
        if group_name in self.groups.keys() and element in self.groups[group_name]:
            self.groups[group_name].remove(element)

    def move_to_back(self, element):
        """Moves given element to back"""
        if element in self.elements:
            self.elements.insert(0, self.elements.pop(self.elements.index(element)))

    def bring_to_front(self, element):
        """Brings given element to front"""
        if element in self.elements:
            self.elements.append(self.elements.pop(self.elements.index(element)))

    def destruct(self):
        """Destructs the manager"""
        for elem in self.elements:
            self.unregister(elem)
