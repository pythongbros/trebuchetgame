from dataclasses import dataclass
import pymunk

from game.objects.templates import PhysicsTemplate
from game.objects.projectile_utils import accelerate, zero_gravity, teleport
from game.objects.collision import CollisionType


@dataclass
class ProjectileTemplate(PhysicsTemplate):
    dimension: tuple = (32, 32)
    image_path: str = "game/resources/objects/basic_projectile.png"
    speed: int = 8
    mass: int = 10
    friction: float = 1.0
    elasticity: float = 0.7
    shape: pymunk.Shape = pymunk.Circle
    action: callable = None
    body_type: pymunk.Body = pymunk.Body.DYNAMIC
    collision_type: CollisionType = CollisionType.PROJECTILE


@dataclass
class LargeProjectileTemplate(ProjectileTemplate):
    dimension: tuple = (48, 48)
    image_path: str = "game/resources/objects/large_projectile.png"
    speed: int = 5
    mass: int = 30


@dataclass
class AcceleratingProjectileTemplate(ProjectileTemplate):
    image_path: str = "game/resources/objects/accelerating_projectile.png"
    action: callable = accelerate


@dataclass
class ZeroGravityProjectileTemplate(ProjectileTemplate):
    image_path: str = "game/resources/objects/zero_gravity_projectile.png"
    action: callable = zero_gravity


@dataclass
class TeleportingProjectileTemplate(ProjectileTemplate):
    image_path: str = "game/resources/objects/teleporting_projectile.png"
    action: callable = teleport
