from dataclasses import dataclass
import pymunk

from game.objects.collision import CollisionType
from game.objects.templates import DestructableTemplate


@dataclass
class BoxTemplate(DestructableTemplate):
    dimension: tuple = (64, 64)
    image_path: str = "game/resources/objects/box_state_00.png"
    mass: int = 10
    friction: float = 1.0
    elasticity: float = 0.4
    shape: pymunk.Shape = pymunk.Poly
    body_type: pymunk.Body = pymunk.Body.DYNAMIC
    collision_type: CollisionType = CollisionType.OBSTACLE

    max_health: int = 100
    initial_health: int = -1
    states: tuple = (
        (40, "game/resources/objects/box_state_02.png"),
        (70, "game/resources/objects/box_state_01.png"),
        (100, "game/resources/objects/box_state_00.png"),
    )
    add_healthbar: bool = False
