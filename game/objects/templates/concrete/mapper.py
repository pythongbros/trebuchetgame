from .aliens_templates import BasicAlienTemplate, ArmoredAlienTemplate
from .ground_template import GroundTemplate
from .block_template import BlockTemplate
from .crate_template import CrateTemplate
from .beam_template import BeamTemplate
from .box_template import BoxTemplate
from .healthbar_template import HealthBarTemplate
from .spaceship_template import SpaceshipTemplate
from .projectiles_templates import (
    ProjectileTemplate,
    LargeProjectileTemplate,
    AcceleratingProjectileTemplate,
    ZeroGravityProjectileTemplate,
    TeleportingProjectileTemplate,
)
from game.objects import (
    SceneObject,
    PhysicsObject,
    DestructableObject,
    Projectile,
    Player,
    Mob,
)

TemplateMapper = {
    "spaceship": SpaceshipTemplate,
    "basic_alien": BasicAlienTemplate,
    "block": BlockTemplate,
    "ground": GroundTemplate,
    "basic_projectile": ProjectileTemplate,
    "large_projectile": LargeProjectileTemplate,
    "accelerating_projectile": AcceleratingProjectileTemplate,
    "zero_gravity_projectile": ZeroGravityProjectileTemplate,
    "teleporting_projectile": TeleportingProjectileTemplate,
    "health_bar": HealthBarTemplate,
    "armored_alien": ArmoredAlienTemplate,
    "box": BoxTemplate,
    "crate": CrateTemplate,
    "beam": BeamTemplate,
}
