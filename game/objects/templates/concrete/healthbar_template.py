from dataclasses import dataclass

from game.objects.templates.valuebar_template import ValueBarTemplate


@dataclass
class HealthBarTemplate(ValueBarTemplate):
    """Basic health bar template"""

    dimension: tuple = (50, 2)
    image_fill_path: str = "game/resources/objects/pb_health_fill.png"
    image_frame_path: str = "game/resources/objects/pb_health_frame.png"
    fill_offset_x: int = 0
    fill_offset_y: int = 0
