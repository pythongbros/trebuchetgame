from dataclasses import dataclass
import pymunk

from game.objects.collision import CollisionType
from game.objects.templates.mob_template import MobTemplate


@dataclass
class BasicAlienTemplate(MobTemplate):
    dimension: tuple = (40, 40)
    mass: int = 20
    image_path: str = "game/resources/objects/alien_state_0.png"
    friction: float = 1.0
    elasticity: float = 0.6
    shape: pymunk.Shape = pymunk.Poly
    body_type: pymunk.Body = pymunk.Body.DYNAMIC
    collision_type: CollisionType = CollisionType.MOB

    max_health: int = 100
    initial_health: int = -1
    states: tuple = (
        (1, "game/resources/objects/alien_state_5.png"),
        (20, "game/resources/objects/alien_state_4.png"),
        (40, "game/resources/objects/alien_state_3.png"),
        (60, "game/resources/objects/alien_state_2.png"),
        (80, "game/resources/objects/alien_state_1.png"),
        (100, "game/resources/objects/alien_state_0.png"),
    )
    add_healthbar: bool = True


@dataclass
class ArmoredAlienTemplate(MobTemplate):
    dimension: tuple = (40, 40)
    mass: int = 20
    image_path: str = "game/resources/objects/alien_armored_state_0.png"
    friction: float = 1.0
    elasticity: float = 0.6
    shape: pymunk.Shape = pymunk.Poly
    body_type: pymunk.Body = pymunk.Body.DYNAMIC
    collision_type: CollisionType = CollisionType.MOB

    max_health: int = 250
    initial_health: int = -1
    states: tuple = (
        (1, "game/resources/objects/alien_armored_state_5.png"),
        (50, "game/resources/objects/alien_armored_state_4.png"),
        (100, "game/resources/objects/alien_armored_state_3.png"),
        (150, "game/resources/objects/alien_armored_state_2.png"),
        (200, "game/resources/objects/alien_armored_state_1.png"),
        (250, "game/resources/objects/alien_armored_state_0.png"),
    )
    add_healthbar: bool = True
