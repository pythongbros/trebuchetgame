from dataclasses import dataclass
import pymunk

from game.objects.collision import CollisionType
from game.objects.templates import DestructableTemplate


@dataclass
class BeamTemplate(DestructableTemplate):
    dimension: tuple = (512, 32)
    image_path: str = "game/resources/objects/beam_state_00.png"
    mass: int = 30
    friction: float = 1.0
    elasticity: float = 0.3
    shape: pymunk.Shape = pymunk.Poly
    body_type: pymunk.Body = pymunk.Body.DYNAMIC
    collision_type: CollisionType = CollisionType.OBSTACLE

    max_health: int = 1300
    initial_health: int = -1
    states: tuple = (
        (200, "game/resources/objects/beam_state_03.png"),
        (400, "game/resources/objects/beam_state_02.png"),
        (700, "game/resources/objects/beam_state_01.png"),
        (1000, "game/resources/objects/beam_state_00.png"),
    )
    add_healthbar: bool = False
