from dataclasses import dataclass
import pymunk

from game.objects.collision import CollisionType
from game.objects.templates import DestructableTemplate


@dataclass
class BlockTemplate(DestructableTemplate):
    dimension: tuple = (64, 64)
    image_path: str = "game/resources/objects/block_state_00.png"
    mass: int = 60
    friction: float = 1.0
    elasticity: float = 0.3
    shape: pymunk.Shape = pymunk.Poly
    body_type: pymunk.Body = pymunk.Body.DYNAMIC
    collision_type: CollisionType = CollisionType.OBSTACLE

    max_health: int = 1500
    initial_health: int = -1
    states: tuple = (
        (500, "game/resources/objects/block_state_03.png"),
        (800, "game/resources/objects/block_state_02.png"),
        (1200, "game/resources/objects/block_state_01.png"),
        (1500, "game/resources/objects/block_state_00.png"),
    )
    add_healthbar: bool = False
