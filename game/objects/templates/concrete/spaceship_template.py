from dataclasses import dataclass

from game.objects.templates.player_template import PlayerTemplate


@dataclass
class SpaceshipTemplate(PlayerTemplate):
    """Player's spaceship template class"""

    dimension: tuple = (192, 192)
    image_path: str = "game/resources/objects/player.png"
    offset: tuple = (0, 0)
    projectile_position: tuple = (0, 132)
