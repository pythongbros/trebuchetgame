from dataclasses import dataclass
import pymunk

from game.objects.collision import CollisionType
from game.objects.templates.physics_template import PhysicsTemplate


@dataclass
class GroundTemplate(PhysicsTemplate):
    dimension: tuple = (256, 16)
    image_path: str = "game/resources/objects/block_ground.png"
    mass: int = 0
    friction: float = 1.0
    elasticity: float = 0.1
    shape: pymunk.Shape = pymunk.Poly
    body_type: pymunk.Body = pymunk.Body.STATIC
    collision_type: CollisionType = CollisionType.WORLD
