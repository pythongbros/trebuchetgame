from dataclasses import dataclass

from game.objects.scene_object import SceneObject


@dataclass
class ValueBarTemplate:
    """Basic structure of ValueBar"""

    position: tuple
    max_value: float
    init_value: float
    parent: SceneObject
    dimension: tuple
    image_fill_path: str
    image_frame_path: str
    fill_offset_x: int
    fill_offset_y: int
