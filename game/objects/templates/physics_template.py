from dataclasses import dataclass
import pymunk

from game.objects.collision.collision_types import CollisionType


@dataclass
class PhysicsTemplate:
    """Basic structure of physics template object"""

    position: tuple
    dimension: tuple
    image_path: str
    mass: int
    friction: float
    elasticity: float
    shape: pymunk.Shape
    body_type: pymunk.Body
    collision_type: CollisionType
