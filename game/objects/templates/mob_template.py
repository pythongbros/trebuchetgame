from dataclasses import dataclass

from game.objects.templates.destructable_template import DestructableTemplate


@dataclass
class MobTemplate(DestructableTemplate):
    """Basic structure of Mob object template"""
