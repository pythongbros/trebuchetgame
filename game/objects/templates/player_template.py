from dataclasses import dataclass


@dataclass
class PlayerTemplate:
    """Player template class"""

    position: tuple
    dimension: tuple
    image_path: str
    offset: tuple
    projectile_position: tuple
