from dataclasses import dataclass

from game.objects.templates.physics_template import PhysicsTemplate


@dataclass
class DestructableTemplate(PhysicsTemplate):
    """Basic structure of DestructableObject template"""

    # Maximum object health
    max_health: int
    # Negative (or zero) initial_health value means start with max_health
    initial_health: int
    # List of pairs: (hp_treshold, image_path), which is used to change
    # object's apperance if the HP falls below given treshold
    states: tuple
    # True if a visual health bar should be added
    add_healthbar: bool
