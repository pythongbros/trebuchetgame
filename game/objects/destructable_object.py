from typing import Tuple

import pyglet
import pymunk
from pymunk.space import Space

from .physics_object import PhysicsObject
from game.objects.collision import CollisionType
from game.objects.templates.concrete.healthbar_template import HealthBarTemplate
from game.objects.templates.destructable_template import DestructableTemplate
from game.objects.value_bar import ValueBar
from game.utils.graphics import scale_image
from game.utils.masks import collision_shape_from_image


class DestructableObject(PhysicsObject):
    """Class representing physical object that can be destroyed during the gameplay.


     Attributes
    ----------
    max_health: int
        Health points limitation for this object

    health: int
        Current number of health points

    states: tuple
        Tuple of pairs: (hp_treshold: int, image: ImageData) which tells the object that it
        should change it's apperance when it's health runs below given treshold

    healthbar: ValueBar
        HealthBar instance used to show object's health. It is set to None if argument
        add_healthbar was set to False when creating this object.

    on_state_change: Callable
        Callback on state change

    on_destroy: Callable
        Callback on object destrucion

    vanish_timeout: float
        Time interval (in seconds) from the point when this object is considered to be 'dead'
        to the point it should be removed from the scene (destoyed).

    """

    def __init__(
        self,
        shape: pymunk.Shape,
        offset: Tuple[int],
        max_health: int,
        init_health: int = -1,
        states: tuple = None,
        zero_gravity: bool = False,
        add_healthbar: bool = False,
        vanish_timeout: float = 0,
    ):
        """Initializator creates DestructableObject from given parameters"""
        self.states = sorted(states, key=lambda x: x[0], reverse=True)
        self._curr_state_index = -1

        super().__init__(shape, self.states[0][1], offset, zero_gravity)

        self.max_health = max_health
        self.health = init_health if init_health > 0 else max_health

        bar_pos = (0, states[-1][1].height / 2)
        self.health_bar = (
            ValueBar.from_template(
                HealthBarTemplate(bar_pos, max_health, self.health, self)
            )
            if add_healthbar
            else None
        )

        self.on_state_change = lambda *_: None
        self.on_destroy = lambda *_: None

        self.vanish_timeout = vanish_timeout

    def set_health(self, hp: int):
        """ Set the health of this object to the given value"""
        self.health = min(self.max_health, max(0, hp))

        for index in range(0, len(self.states) - 1):
            if self.health > self.states[index + 1][0]:
                break
            else:
                index += 1

        if self._curr_state_index != index:
            self.image = self.states[index][1]
            self._curr_state_index = index

            self.image.anchor_x = self.image.width / 2
            self.image.anchor_y = self.image.height / 2

            self.on_state_change()

        if self.health_bar is not None:
            self.health_bar.fetch_value(self.health, self.max_health)

        if not self.is_alive():
            pyglet.clock.schedule_once(self._destroy_timeout, self.vanish_timeout)

    def heal(self, hp: int):
        """ Add some health points for this object """
        self.set_health(self.health + hp)

    def hit(self, hp: int):
        """ Remove some health points for this object """
        self.set_health(self.health - hp)

    def collhandler_post_solve(self, arbiter, space: Space, data):
        """Defines behavoiur on collision with other object"""
        if self.shape in arbiter.shapes:
            colliding_shape = (
                arbiter.shapes[1]
                if self.shape == arbiter.shapes[0]
                else arbiter.shapes[0]
            )

            if colliding_shape.collision_type == CollisionType.PROJECTILE.value:
                self.hit(arbiter.total_impulse.get_length() // 100)
            elif colliding_shape.collision_type in [
                CollisionType.WORLD.value,
                CollisionType.OBSTACLE.value,
                CollisionType.MOB.value,
            ]:
                damage = arbiter.total_ke // 300000
                if damage > 5:
                    self.hit(damage)

        super().collhandler_post_solve(arbiter, space, data)
        return True

    def is_alive(self):
        """Defines if an object is considered as alive"""
        if self.health <= 0:
            return False
        return True

    def _destroy_timeout(self, dt):
        """Called when the vanish_timeout passed and sets the destory flag"""
        self.destroyed = True
        self.on_destroy()

    @staticmethod
    def init_shape(template: DestructableTemplate):
        """Method creating body and shape from given template"""
        body = pymunk.Body(body_type=template.body_type)
        body.position = template.position

        states = [
            (s[0], scale_image(pyglet.image.load(s[1]), *template.dimension))
            for s in template.states
        ]

        image = pyglet.image.load(template.image_path)
        image = scale_image(image, *template.dimension)

        if template.shape == pymunk.Circle:
            if template.dimension[0] != template.dimension[1]:
                raise ValueError(
                    f"Dimension of circle needs to be equal in both dimensions, given: {template.dimension}"
                )

            radius = template.dimension[0] // 2
            shape = pymunk.Circle(body, radius, (radius, -radius))
            offset = 0, 0

        elif template.shape == pymunk.Poly:
            shape, offset = collision_shape_from_image(
                template.image_path, template.dimension, body
            )
        else:
            raise ValueError(
                f"Shape needs to be a pymunk.Shape (Circle or Poly) not {template.shape}"
            )

        shape.mass = template.mass
        shape.friction = template.friction
        shape.elasticity = template.elasticity
        shape.collision_type = template.collision_type.value
        shape.cache_bb()

        return shape, states, offset

    @classmethod
    def from_template(cls, template: DestructableTemplate):
        """Class method creating destructable object from given template"""
        shape, states, offset = DestructableObject.init_shape(template)

        return cls(
            shape,
            offset,
            template.max_health,
            template.initial_health,
            states,
            False,
            template.add_healthbar,
        )
