import math
from typing import Tuple

import pyglet
from pyglet.sprite import Sprite
from pyglet.image import ImageData

import pymunk

from game.utils.masks import collision_shape_from_image
from game.objects.templates import PhysicsTemplate
from game.utils.graphics import scale_image
from game.objects.scene_object import SceneObject
from game.objects.colliding_object import CollidingObject


class PhysicsObject(SceneObject, CollidingObject):
    """Basic physics object class handling pymunk objects

    This class is a basic scheme building pymunk body and shape
    objects from given parameters. Object can be created straight out of
    a given shape with attached body but the better way is to use
    classmethod from_template() which builds body and shape
    for the user from given set of parameters. Templates for such objects
    can be seen in templates directory.

    Attributes
    ----------
    shape: pymunk.shape
        Shape of physics object with attached body.

    zero_gravity: bool
        Whether a gravity of space should affect this object (it doesn't affect static bodies anyways).
    """

    def __init__(
        self,
        shape: pymunk.Shape,
        image: ImageData,
        offset: Tuple[int],
        zero_gravity: bool = False,
        register_as_projectile: bool = False,
    ):
        SceneObject.__init__(self, image, offset)
        CollidingObject.__init__(self)

        self.shape = shape
        self.shape.cache_bb()

        self.zero_gravity = zero_gravity

        if self.shape.body.body_type != pymunk.Body.STATIC:
            self.shape.body.velocity_func = self.custom_velocity

    def custom_velocity(self, body, gravity, damping, dt):
        """Overide of pymunk body velocity function to allow zero gravity"""
        if self.zero_gravity:
            pymunk.Body.update_velocity(body, (0, 0), damping, dt)
        else:
            pymunk.Body.update_velocity(body, gravity, damping, dt)

        velocity = body.velocity

        if abs(velocity.x) < 3:
            velocity.x = 0.0

        body.velocity = velocity

    def is_point_inside(self, x, y):
        """Returns True if given point is inside the bounding box of this element"""
        return (
            self.shape.bb.left <= x <= self.shape.bb.right
            and self.shape.bb.bottom <= y <= self.shape.bb.top
        )

    def set_position(self, x, y):
        """Sets position of the object"""
        rot = self.shape.body.angle
        self.shape.body.angle = 0
        self.shape.cache_bb()
        width = self.shape.bb.right - self.shape.bb.left
        height = self.shape.bb.top - self.shape.bb.bottom

        self.shape.body.position = (x - width / 2, y + height / 2)
        self.shape.body.angle = rot
        self.shape.cache_bb()

    def set_sprite_position(self, x, y):
        """Sets position of the object by sprite coordinates"""
        self.shape.body.position = x, y
        self.shape.cache_bb()
        Sprite.update(self, x=x, y=y)

    def set_rotation(self, angle):
        """Sets rotation of the object"""
        self.shape.body.angle = angle
        self.shape.cache_bb()

    def rotate(self, angle):
        """Applies additional rotation of the object"""
        self.shape.body.angle += angle
        self.shape.cache_bb()

    def get_absolute_position(self):
        """Get the position regardless of it's parent"""
        x = self.shape.bb.center()[0] - self.offset[0]
        y = self.shape.bb.center()[1] - self.offset[1]

        if self.parent is not None:
            x += self.parent.get_absolute_position()[0]
            y += self.parent.get_absolute_position()[1]

        return x, y

    def update(self):
        x, y = self.get_absolute_position()
        Sprite.update(self, x=x, y=y, rotation=math.degrees(-self.shape.body.angle))

        super().update()

    @staticmethod
    def init_shape(template: PhysicsTemplate):
        """Method creating body and shape from given template"""
        body = pymunk.Body(body_type=template.body_type)
        body.position = template.position

        image = pyglet.image.load(template.image_path)
        image = scale_image(image, *template.dimension)

        if template.shape == pymunk.Circle:
            if template.dimension[0] != template.dimension[1]:
                raise ValueError(
                    f"Dimension of circle needs to be equal in both dimensions, given: {template.dimension}"
                )

            radius = template.dimension[0] // 2
            shape = pymunk.Circle(body, radius, (radius, -radius))
            offset = 0, 0

        elif template.shape == pymunk.Poly:
            shape, offset = collision_shape_from_image(
                template.image_path, template.dimension, body
            )
        else:
            raise ValueError(
                f"Shape needs to be a pymunk.Shape (Circle or Poly) not {template.shape}"
            )

        shape.mass = template.mass
        shape.friction = template.friction
        shape.elasticity = template.elasticity
        shape.collision_type = template.collision_type.value
        shape.cache_bb()

        return shape, image, offset

    @classmethod
    def from_template(cls, template: PhysicsTemplate):
        """Class method creating physics object from given template"""
        shape, image, offset = PhysicsObject.init_shape(template)
        return cls(shape, image, offset)
