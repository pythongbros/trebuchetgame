from typing import Tuple

from pyglet.sprite import Sprite
from pyglet.image import ImageData


class SceneObject(Sprite):
    """Represents an object on a scene

    This is a simple extension of pyglet's Sprite class.
    Notice that GUIElements take as anchor their lower left corner,
    however SceneObjects anchoring is based on the center of their
    bounding box.

    Attributes
    ----------
    image: ImageData
        A graphical representation of this object

    offset: Tuple[int]
        Offset to be applied to image on rendering

    children : list
            List of this object's children.

    parent : SceneObject
            Parent of this object

    destroyed: bool
        True if object should be removed from the scene.

    permament: bool
        True if object shouldn't be removed from the scene at all.

    """

    def __init__(
        self,
        image: ImageData,
        offset: Tuple[int],
    ):
        """Initializator creates a SceneObject from given parameters and registers it"""
        super().__init__(image)

        self.offset = offset

        self.children = []
        self.parent = None

        self.image.anchor_x = self.image.width / 2
        self.image.anchor_y = self.image.height / 2

        self.destroyed = False
        self.permament = False

    def add_child(self, child):
        """Adds a new child to this object"""
        """ IMPORTANT: Always prevent looping child-parent relations!!!"""

        if child not in self.children and self not in child.children:
            child.parent = self
            self.children.append(child)

    def remove_child(self, child):
        """Removes given object from children list"""
        if child in self.children:
            child.parent = None
            self.children.remove(child)

    def is_point_inside(self, x, y):
        """Returns True if given point is inside the bounding box of this element"""
        # Since we don't have any shapes here, we will use image dimensions:
        left, right = (
            self.position[0] - self.image.width / 2,
            self.position[0] + self.image.width / 2,
        )
        top, bottom = (
            self.position[1] + self.image.height / 2,
            self.position[1] - self.image.height / 2,
        )

        return left <= x <= right and bottom <= y <= top

    def set_position(self, x, y):
        """Sets position of the object"""
        self.position = x, y

    def set_rotation(self, angle):
        """Sets rotation of the object"""
        raise NotImplementedError

    def rotate(self, angle):
        """Applies additional rotation of the object"""
        raise NotImplementedError

    def get_absolute_position(self):
        """Get the position regardless of it's parent"""
        raise NotImplementedError

    def draw(self):
        """Draws this object on the screen"""
        super().draw()

    def draw_all(self):
        """Draws this object and it's children on the screen"""
        self.draw()

        for child in self.children:
            child.draw_all()

    def update(self):
        """Update function required for each object"""
        super().update()

    def update_all(self):
        """Called every frame, realizes the behaviour of this object and all of it's children"""
        self.update()

        for child in self.children:
            child.update()
