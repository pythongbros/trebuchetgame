from typing import Tuple

import pyglet
from pyglet.image import ImageData
from pyglet.window import mouse

import pymunk
from pymunk.vec2d import Vec2d

from .projectile_utils import Trajectory
from .physics_object import PhysicsObject
from .templates import PhysicsTemplate


class Projectile(PhysicsObject):
    """Class for projectile interactable with user.


    Attributes
    ----------
    shape: pymunk.shape
        Shape of physics object with attached body

    speed: int
        Speed of given projectile - basiacally more speed less force required to make it move.
        Mass doesn't really interact in this way since it only is applied on collision.

    focus: bool
        Whether this projectile can interact with user - there can't be more than two

    zero_gravity: bool
        Whether a gravity of space should affect this object (it doesn't affect static bodies anyways)

    vanish_timeout: float
        Time interval (in seconds) from the point when this object is considered to be 'dead'
        to the point it should be removed from the scene (destoyed).
    """

    def __init__(
        self,
        shape: pymunk.Shape,
        image: ImageData,
        offset: Tuple[int],
        speed: int,
        action: callable,
        zero_gravity: bool = True,
    ):
        super().__init__(shape, image, offset, zero_gravity)
        self.shape.body.velocity_func = self.custom_velocity
        self.speed = speed
        self.action = action
        self.shot = False

        self.trajectory = Trajectory()
        self.vanish_timeout = 3

    def draw(self):
        super().draw()
        self.trajectory.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        """On mouse press handler for projectile"""

        if button == mouse.LEFT:
            if self.shot:
                if self.action is not None:
                    self.action(self, x, y)
                    self.action = None
            else:
                self.trajectory.show = True
        elif button == mouse.RIGHT:
            self.trajectory.show = False

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        """On mouse drag handler for projectile - update trajectory if not shot yet"""
        if not self.shot and self.trajectory.show:
            x2, y2 = self.get_center()
            self.trajectory.update_position(x, y, x2, y2)

    def on_mouse_release(self, x, y, button, modifiers):
        """On mouse release handler"""
        if button == mouse.LEFT and not self.shot and self.trajectory.show:
            self.shoot(Vec2d(x, y))
            self.shot = True
            self.trajectory.show = False

    def shoot(self, point: Vec2d):
        """Shoot function applying given vector to velocity of object"""
        direction = self.speed * (self.shape.body.position - point)
        self.zero_gravity = False
        self.shape.body.velocity = direction

    def get_center(self):
        """Returns center of projectile for various reasons"""
        dimension = (self.image.width, self.image.height)
        x = self.shape.body.position.x + (dimension[0] // 2)
        y = self.shape.body.position.y - (dimension[1] // 2)

        return x, y

    def collhandler_begin(self, arbiter, space, data):
        """On begin collision handler"""
        if self.shape in arbiter.shapes:
            self.action = None

        return True

    def is_alive(self):
        """Defines if an object is considered as alive"""
        if self.action is None and self.shot and self.shape.body.velocity.length < 25:
            return False
        return True

    def _destroy_timeout(self, dt):
        """Called when the vanish_timeout passed and sets the destory flag"""
        self.destroyed = True

    def update(self):
        super().update()
        if not self.is_alive():
            pyglet.clock.schedule_once(self._destroy_timeout, self.vanish_timeout)

    @classmethod
    def from_template(cls, template: PhysicsTemplate):
        """Class method creating projectile object from given template"""
        shape, image, offset = super().init_shape(template)
        return cls(shape, image, offset, template.speed, template.action)
