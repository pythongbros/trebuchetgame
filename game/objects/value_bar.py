from typing import Tuple

import pyglet
from pyglet.image import ImageData
from pyglet.sprite import Sprite

from game.utils.graphics import scale_image
from game.objects.templates.valuebar_template import ValueBarTemplate
from game.objects.scene_object import SceneObject


class ValueBar(SceneObject):
    """This is an object that provides a visual way to represent the specified value of an object

    It works as an observer - waits for notification about a change of the observed value
    and then fills the bar to depict that change.

    Attributes
    ----------
    position : Tuple[int]
        Position of this object

    _src_img_frame: ImageData
        Original image of the bar frame
    _src_img_fill: ImageData
        Original image of the bar filling
    img_fill_scaled: ImageData
        Scaled image of the bar filling

    _origin_offset_x : int
        Original pixel offset of the filling from the frame along the x axis
    _origin_offset_y : int
        Original pixel offset of the filling from the frame along the y axis
    """

    def __init__(
        self,
        position: Tuple[int],
        img_frame: ImageData,
        img_fill: ImageData,
        max_value: float,
        init_value: float,
        fill_offset_x: int,
        fill_offset_y: int,
        parent: None,
    ):
        """Initializator creates the bar from given parameters."""
        self._origin_offset_x = fill_offset_x
        self._origin_offset_y = fill_offset_y

        self._src_img_frame = img_frame

        self._src_img_fill = img_fill

        self._percentage = init_value / max_value

        super().__init__(img_fill, (0, 0))

        self.rel_position = position

        if parent is not None:
            parent.add_child(self)

        self.fetch_value(init_value, max_value)

    def _merge_images(self, fill: ImageData):
        """Merges the frame and fill images to obtain the complete object"""
        tex_fill = fill.get_image_data()
        tex_frame = self._src_img_frame.get_image_data()

        texture = pyglet.image.Texture.create(
            width=tex_frame.width, height=tex_frame.height
        )

        texture.blit_into(tex_frame, x=0, y=0, z=0)
        texture.blit_into(tex_fill, self._origin_offset_x, self._origin_offset_y, z=1)

        return texture.get_image_data()

    def get_absolute_position(self):
        """Get the position regardless of it's parent"""
        x = self.rel_position[0] - self.offset[0]
        y = self.rel_position[1] - self.offset[1]

        if self.parent is not None:
            x += self.parent.get_absolute_position()[0]
            y += self.parent.get_absolute_position()[1]

        return x, y

    def fetch_filling(self):
        """Fetches the bar filling to represent the current percentage"""
        fill = pyglet.image.Texture.create(
            width=self._src_img_fill.width, height=self._src_img_fill.height
        )
        fill.blit_into(self._src_img_fill, x=0, y=0, z=0)

        fill = scale_image(
            fill,
            int(self._src_img_fill.width * self._percentage),
            self._src_img_fill.height,
        )

        self.image = self._merge_images(fill)

    def fetch_value(self, progress_val: float, max_value: float):
        """Fetch the progress value from the observable"""
        self._percentage = progress_val / max_value
        self.fetch_filling()

    def draw(self):
        """Draws this object on the screen"""
        self.image.anchor_x = self.image.width / 2
        self.image.anchor_y = self.image.height / 2

        x, y = self.get_absolute_position()
        Sprite.update(self, x=x, y=y, rotation=None)

        super().draw()

    @classmethod
    def from_template(cls, template: ValueBarTemplate):
        """Class method creating physics object from given template"""
        img_frame = scale_image(
            pyglet.image.load(template.image_frame_path), *template.dimension
        )
        img_fill = scale_image(
            pyglet.image.load(template.image_fill_path), *template.dimension
        )

        return cls(
            template.position,
            img_frame,
            img_fill,
            template.max_value,
            template.init_value,
            template.fill_offset_x,
            template.fill_offset_y,
            template.parent,
        )
