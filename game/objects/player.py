from typing import Tuple

import pyglet
from pyglet.sprite import Sprite
from pyglet.image import ImageData

from game.utils.graphics import scale_image
from .templates.player_template import PlayerTemplate
from .scene_object import SceneObject


class Player(SceneObject):
    """This class resembles the player's spaceship.

    It defines where the projectiles are thrown from
    and determines the look of the player's spaceship.

    Attributes:
    -----------
    position: Tuple[int]
        Position of the player
    projectile_pos: Tuple[int]
        Relative position of the projectile to be thrown from
    """

    def __init__(
        self,
        position: Tuple[int],
        image: ImageData,
        offset: Tuple[int],
        projectile_position: Tuple[int],
    ):
        super().__init__(image, offset)
        self.image.anchor_x = self.image.width / 2
        self.image.anchor_y = self.image.height / 2

        self.projectile_pos = projectile_position
        self.position = position

    def get_absolute_position(self):
        """Get the position regardless of it's parent"""
        x = self.position[0] - self.offset[0]
        y = self.position[1] - self.offset[1]

        if self.parent is not None:
            x += self.parent.get_absolute_position()[0]
            y += self.parent.get_absolute_position()[1]

        return x, y

    def get_absolute_projectile_position(self):
        """Returns the absolute position of the projectile spot"""
        return (
            self.get_absolute_position()[0] + self.projectile_pos[0],
            self.get_absolute_position()[1] + self.projectile_pos[1],
        )

    def set_position(self, x, y):
        """Sets position of the object"""
        self.position = x, y

    def draw(self):
        """Draws this object on the screen"""
        x, y = self.get_absolute_position()
        Sprite.update(self, x=x, y=y, rotation=None)

        super().draw()

    @classmethod
    def from_template(cls, template: PlayerTemplate):
        """Class method creating physics object from given template"""
        img_player = scale_image(
            pyglet.image.load(template.image_path), *template.dimension
        )

        return cls(
            template.position,
            img_player,
            template.offset,
            template.projectile_position,
        )
