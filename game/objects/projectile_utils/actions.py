def accelerate(projectile, x, y):
    projectile.shape.body.velocity *= 2


def zero_gravity(projectile, x, y):
    projectile.zero_gravity = True


def teleport(projectile, x, y):
    projectile.shape.body.position = (
        x + projectile.image.width,
        y - projectile.image.height,
    )
