from pyglet import shapes


class Trajectory(shapes.Line):
    """Trajectory class to draw trajectory line when aiming.

    The line is being drawn between points (x, y) and (x2, y2).

    Attributes
    ----------
    x : int
        x coordinate of first point

    y : int
        y coordinate of first point

    x2 : int
        x coordinate of second point

    y2 : int
        y coordinate of second point

    width : int
        Width of line

    color : tuple
        Color encoded in RGB
    """

    def __init__(
        self,
        x: int = 0,
        y: int = 0,
        x2: int = 0,
        y2: int = 0,
        width: int = 3,
        color: tuple = (50, 225, 30),
    ):
        super().__init__(x, y, x2, y2, width, color)
        self.opacity = 250
        self.show = False

    def update_position(self, x, y, x2, y2):
        self.x = x
        self.y = y
        self.x2 = x2
        self.y2 = y2

    def draw(self):
        if self.show:
            super().draw()
