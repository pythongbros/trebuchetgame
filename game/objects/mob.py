from typing import Tuple

import pyglet
import pymunk

from game.objects.templates import MobTemplate
from game.objects import DestructableObject
from game.utils.graphics import scale_image
from game.utils.masks import collision_shape_from_image


class Mob(DestructableObject):
    """Class representing a mob that can be killed by a projectile.


     Attributes
    ----------

    """

    def __init__(
        self,
        shape: pymunk.Shape,
        offset: Tuple[int],
        max_health: int,
        init_health: int = -1,
        states: tuple = None,
    ):
        """Initializator creates DestructableObject from given parameters"""
        super().__init__(shape, offset, max_health, init_health, states, False, True, 3)

    @staticmethod
    def init_shape(template: MobTemplate):
        """Method creating body and shape from given template"""
        body = pymunk.Body(body_type=template.body_type)
        body.position = template.position

        states = [
            (s[0], scale_image(pyglet.image.load(s[1]), *template.dimension))
            for s in template.states
        ]

        image = pyglet.image.load(template.image_path)
        image = scale_image(image, *template.dimension)

        if template.shape == pymunk.Circle:
            if template.dimension[0] != template.dimension[1]:
                raise ValueError(
                    f"Dimension of circle needs to be equal in both dimensions, given: {template.dimension}"
                )

            radius = template.dimension[0] // 2
            shape = pymunk.Circle(body, radius, (radius, -radius))
            offset = 0, 0

        elif template.shape == pymunk.Poly:
            shape, offset = collision_shape_from_image(
                template.image_path, template.dimension, body
            )
        else:
            raise ValueError(
                f"Shape needs to be a pymunk.Shape (Circle or Poly) not {template.shape}"
            )

        shape.mass = template.mass
        shape.friction = template.friction
        shape.elasticity = template.elasticity
        shape.collision_type = template.collision_type.value
        shape.cache_bb()

        return shape, states, offset

    @classmethod
    def from_template(cls, template: MobTemplate):
        """Class method creating destructable object from given template"""
        shape, states, offset = Mob.init_shape(template)

        return cls(shape, offset, template.max_health, template.initial_health, states)
