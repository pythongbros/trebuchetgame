from game.objects.colliding_object import CollidingObject


class CollisionHandler:
    """This is the collision handler class cooperating with Object Manager to manage collisions

    Attributes
    ----------
    handlers_begin: list
        List of all registered begin handlers
    handlers_pre_solve: list
        List of all registered pre_solve handlers
    handlers_post_solve: list
        List of all registered post_solve handlers
    handlers_separate: list
        List of all registered separate handlers
    """

    def __init__(self):
        self.handlers_begin = []
        self.handlers_pre_solve = []
        self.handlers_post_solve = []
        self.handlers_separate = []

    def mount_handlers(self, space_ref: list):
        """Mounts handler functions into the space"""
        h = space_ref[0].add_default_collision_handler()
        h.begin = self._handler_begin
        h.pre_solve = self._handler_pre_solve
        h.post_solve = self._handler_post_solve
        h.separate = self._handler_separate

    def dismount_handlers(self, space_ref: list):
        """Dismounts handler functions into the space"""
        h = space_ref[0].add_default_collision_handler()
        h.begin = None
        h.pre_solve = None
        h.post_solve = None
        h.separate = None

    def mount_object(self, obj: CollidingObject):
        """Mounts given object's collision handlers"""
        self.handlers_begin.append(obj.collhandler_begin)
        self.handlers_pre_solve.append(obj.collhandler_pre_solve)
        self.handlers_post_solve.append(obj.collhandler_post_solve)
        self.handlers_separate.append(obj.collhandler_separate)

    def dismount_object(self, obj: CollidingObject):
        """Dismounts given object's collision handlers"""
        if obj.collhandler_begin in self.handlers_begin:
            self.handlers_begin.remove(obj.collhandler_begin)
        if obj.collhandler_pre_solve in self.handlers_begin:
            self.handlers_pre_solve.remove(obj.collhandler_pre_solve)
        if obj.collhandler_post_solve in self.handlers_begin:
            self.handlers_post_solve.remove(obj.collhandler_post_solve)
        if obj.collhandler_separate in self.handlers_begin:
            self.handlers_separate.remove(obj.collhandlere.separate)

    def _handler_begin(self, arbiter, space, data):
        """On begin collision handler"""
        for handler in self.handlers_begin:
            handler(arbiter, space, data)

        return True

    def _handler_pre_solve(self, arbiter, space, data):
        """On pre-solve collision handler"""
        for handler in self.handlers_pre_solve:
            handler(arbiter, space, data)

        return True

    def _handler_post_solve(self, arbiter, space, data):
        """On post-solve collision handler"""
        for handler in self.handlers_post_solve:
            handler(arbiter, space, data)

        return True

    def _handler_separate(self, arbiter, space, data):
        """On separate collision handler"""
        for handler in self.handlers_separate:
            handler(arbiter, space, data)

        return True
