from enum import Enum


class CollisionType(Enum):
    """Here is the list of all pymunk collison types provided in the game"""

    """
        WORLD represents every world element that is a static part of the environment
        and cannot be moved or destroyed.
    """
    WORLD = 0

    """
        OBSTACLE represents every obstacle on the map, which
        can be moved or sometimes even destroyed.
    """
    OBSTACLE = 1

    """
        MOB represents every mob - a creature that is a dynamic object and
        can be killed during the gameplay.
    """
    MOB = 2

    """
    PROJECTILE represents a projectile shot from player's spaceship
    """
    PROJECTILE = 3
