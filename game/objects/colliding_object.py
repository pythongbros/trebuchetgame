class CollidingObject:
    """An interface to inherit from if an object should provide collisions

    It defines all handlers needed for collision handling.

    """

    def collhandler_begin(self, arbiter, space, data):
        """On begin collision handler"""
        return True

    def collhandler_pre_solve(self, arbiter, space, data):
        """On pre-solve collision handler"""
        return True

    def collhandler_post_solve(self, arbiter, space, data):
        """On post-solve collision handler"""
        return True

    def collhandler_separate(self, arbiter, space, data):
        """On separate collision handler"""
        return True
