.. trebuchetgame documentation master file, created by
   sphinx-quickstart on Mon Oct 19 21:32:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to trebuchetgame's documentation!
=========================================

Documentation for the Code
**************************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: game


Main game module
=========================

.. automodule:: game.main
   :members: start_app

.. autoclass:: game.gui.Button
   :members:
   :undoc-members:
   :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
