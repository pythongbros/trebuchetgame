# TrebuchetGame

Project for course "Programowanie zespołowe" on Wrocław University of Science and Technology. Developed project will be a 2D game in python a'la AngryBirds with WebService for game community.
When donwloaded run: `python -m game.main`

### Conda environment
To setup conda environment on Windows run: `conda env create -f environment.yml`.

### Webservice environment 
#### Frontend app 
To setup frontend app environment navigate to `webservice/frontned` 
and run: `npm install`.
#### Frontend app 
To setup frontend app environment navigate to `webservice/backend` 
and run: `npm install`.

### Docs
To generate python docs run: `make html` in `docs` directory. Than navigate to `docs/build/html/index.html` to open generated documentation in browser. 

To generate frontend app docs navigate to `webservice/frontend`  and run: `npm run docs`
To generate frontend app docs navigate to `webservice/backend`  and run: `npm run docs`